// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   BeanReadOnlyProperty.java

package es.di.framework.commons.xstream;

import com.thoughtworks.xstream.converters.javabean.BeanProperty;
import java.lang.reflect.Method;

public class BeanReadOnlyProperty extends BeanProperty {

	public BeanReadOnlyProperty(Class memberClass, String propertyName, Class propertyType) {
		super(memberClass, propertyName, propertyType);
	}

	public Method getGetterMethod() {
		return getter;
	}
}
