// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   JavaBeanReadOnlyConverter.java

package es.di.framework.commons.xstream;

//import com.thoughtworks.xstream.alias.ClassMapper;
// JMH (25.09.2020): Deprecate ClassMapper for Mapper. All methods with a ClassMapper parameter have now a duplicate taking only a Mapper. The variant with the ClassMapper is deprecated.
import com.thoughtworks.xstream.mapper.Mapper;

import com.thoughtworks.xstream.converters.*;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

// Referenced classes of package es.di.framework.commons.xstream:
//			BeanReadOnlyProvider

public class JavaBeanReadOnlyConverter
	implements Converter {
	public static class DuplicateFieldException extends ConversionException {

		private static final long serialVersionUID = 1L;

		public DuplicateFieldException(String msg) {
			super(msg);
		}
	}


	protected static final Log log = LogFactory.getLog(es.di.framework.commons.xstream.JavaBeanReadOnlyConverter.class);
	
	// JMH (25.09.2020): Deprecate ClassMapper for Mapper
	//private ClassMapper classMapper = null;
	private Mapper classMapper = null;
	private String classAttributeIdentifier = null;
	private BeanReadOnlyProvider beanProvider = null;

	// JMH (25.09.2020): Deprecate ClassMapper for Mapper
	//public JavaBeanReadOnlyConverter(ClassMapper classMapper, String classAttributeIdentifier) {
	public JavaBeanReadOnlyConverter(Mapper classMapper, String classAttributeIdentifier) {
		this.classMapper = classMapper;
		this.classAttributeIdentifier = classAttributeIdentifier;
		beanProvider = new BeanReadOnlyProvider();
	}

	public boolean canConvert(Class type) {
		return beanProvider.canInstantiate(type);
	}

	public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context) {
		this.beanProvider.visitSerializableProperties(
			source,
			new BeanReadOnlyProvider.Visitor() {
				/*final HierarchicalStreamWriter val$writer = null;
				final Object val$source = null;
				final MarshallingContext val$context = null;
				final JavaBeanReadOnlyConverter this$0 = null; 
				{
					this$0 = JavaBeanReadOnlyConverter.this;
					writer = hierarchicalstreamwriter;
					source = obj;
					context = marshallingcontext;
					super();
				}
				{
					this$0 = JavaBeanReadOnlyConverter.this;
					//val$writer = writer;
					val$source = source;
					val$context = context;

				}*/
				public void visit(String propertyName, Class fieldType, Object newObj) {
					if (newObj != null) writeField(propertyName, fieldType, newObj);
				}
				private void writeField(String propertyName, Class fieldType, Object newObj) {
					writer.startNode(classMapper.serializedMember(source.getClass(), propertyName));
					Class actualType = newObj.getClass();
					Class defaultType = classMapper.defaultImplementationOf(fieldType);
					if (!actualType.equals(defaultType))
						writer.addAttribute(classAttributeIdentifier, classMapper.serializedClass(actualType));
					context.convertAnother(newObj);
					writer.endNode();
				}
			}
		);
	}

	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		Object result = instantiateNewInstance(context);
		for (; reader.hasMoreChildren(); reader.moveUp()) {
			reader.moveDown();
			Class clazz = result.getClass();
			String propertyName = classMapper.realMember(clazz, reader.getNodeName());
			boolean propertyExistsInClass = beanProvider.propertyWriteableDefinedInClass(propertyName, clazz);
			if (propertyExistsInClass) {
				Class type = determineType(reader, result, propertyName);
				Object value = context.convertAnother(result, type);
				beanProvider.writeProperty(result, propertyName, value);
				continue;
			}
			if (log.isWarnEnabled())
				log.warn((new StringBuilder()).append("La propiedad ").append(propertyName).append(" no tiene SETTER definido en la clase ").append(clazz.getName()).toString());
		}

		return result;
	}

	private Object instantiateNewInstance(UnmarshallingContext context) {
		Object result = context.currentObject();
		if (result == null)
			result = beanProvider.newInstance(context.getRequiredType());
		return result;
	}

	private Class determineType(HierarchicalStreamReader reader, Object result, String fieldName) {
		String classAttribute = reader.getAttribute(classAttributeIdentifier);
		if (classAttribute != null)
			return classMapper.realClass(classAttribute);
		else
			return classMapper.defaultImplementationOf(beanProvider.getPropertyType(result, fieldName));
	}



}
