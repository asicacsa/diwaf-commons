// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   PropertyReadOnlyDictionary.java

package es.di.framework.commons.xstream;

import com.thoughtworks.xstream.converters.javabean.BeanProperty;
import com.thoughtworks.xstream.converters.reflection.ObjectAccessException;
import java.beans.Introspector;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

// Referenced classes of package es.di.framework.commons.xstream:
//			BeanReadOnlyProperty

public class PropertyReadOnlyDictionary {
	private static class OrderRetainingMap extends HashMap {

		private static final long serialVersionUID = 1L;
		private List valueOrder = null;

		public Object put(Object key, Object value) {
			valueOrder.add(value);
			return super.put(key, value);
		}

		public Collection values() {
			return Collections.unmodifiableList(valueOrder);
		}

		private OrderRetainingMap() {
			valueOrder = new ArrayList();
		}

	}

	private static class BeanPropertyComparator
		implements Comparator {

		public int compare(Object o1, Object o2) {
			return ((BeanProperty)o1).getName().compareTo(((BeanProperty)o2).getName());
		}

		private BeanPropertyComparator() {
		}

	}

	private static class PropertyKey {

		private String propertyName = null;
		private Class propertyType = null;

		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(o instanceof PropertyKey))
				return false;
			PropertyKey propertyKey = (PropertyKey)o;
			if (propertyName == null ? propertyKey.propertyName != null : !propertyName.equals(propertyKey.propertyName))
				return false;
			return propertyType == null ? propertyKey.propertyType == null : propertyType.equals(propertyKey.propertyType);
		}

		public int hashCode() {
			int result = propertyName == null ? 0 : propertyName.hashCode();
			result = 29 * result + (propertyType == null ? 0 : propertyType.hashCode());
			return result;
		}

		public String toString() {
			return (new StringBuilder()).append("PropertyKey{propertyName='").append(propertyName).append("'").append(", propertyType=").append(propertyType).append("}").toString();
		}

		public PropertyKey(String propertyName, Class propertyType) {
			this.propertyName = propertyName;
			this.propertyType = propertyType;
		}
	}


	protected static final Log log = LogFactory.getLog(es.di.framework.commons.xstream.PropertyReadOnlyDictionary.class);
	private final Map keyedByPropertyNameCache = Collections.synchronizedMap(new HashMap());

	public PropertyReadOnlyDictionary() {
	}

	public Iterator serializablePropertiesFor(Class cls) {
		return buildMap(cls).values().iterator();
	}

	private BeanProperty getBeanProperty(Map propertyMap, Class cls, String propertyName, Class type) {
		PropertyKey key = new PropertyKey(propertyName, type);
		BeanProperty property = (BeanProperty)propertyMap.get(key);
		if (property == null) {
			property = new BeanReadOnlyProperty(cls, propertyName, type);
			propertyMap.put(key, property);
		}
		return property;
	}

	public BeanProperty property(Class cls, String name) {
		Map properties = buildMap(cls);
		BeanProperty property = (BeanProperty)properties.get(name);
		if (property == null)
			throw new ObjectAccessException((new StringBuilder()).append("No such property ").append(cls.getName()).append(".").append(name).toString());
		else
			return property;
	}

	private Map buildMap(Class cls) {
		String clsName = cls.getName();
		if (!keyedByPropertyNameCache.containsKey(clsName))
			synchronized (keyedByPropertyNameCache) {
				if (!keyedByPropertyNameCache.containsKey(clsName)) {
					Map propertyMap = new HashMap();
					Method methods[] = cls.getMethods();
					for (int i = 0; i < methods.length; i++) {
						if (!Modifier.isPublic(methods[i].getModifiers()) || Modifier.isStatic(methods[i].getModifiers()))
							continue;
						String methodName = methods[i].getName();
						Class parameters[] = methods[i].getParameterTypes();
						Class returnType = methods[i].getReturnType();
						String propertyName = null;
						if ((methodName.startsWith("get") || methodName.startsWith("is")) && parameters.length == 0 && returnType != null) {
							if (methodName.startsWith("get"))
								propertyName = Introspector.decapitalize(methodName.substring(3));
							else
								propertyName = Introspector.decapitalize(methodName.substring(2));
							BeanProperty property = getBeanProperty(propertyMap, cls, propertyName, returnType);
							property.setGetterMethod(methods[i]);
							continue;
						}
						if (!methodName.startsWith("set") || parameters.length != 1 || !returnType.equals(Void.TYPE))
							continue;
						if (parameters[0].isInterface() || !Modifier.isAbstract(parameters[0].getModifiers())) {
							propertyName = Introspector.decapitalize(methodName.substring(3));
							BeanProperty property = getBeanProperty(propertyMap, cls, propertyName, parameters[0]);
							property.setSetterMethod(methods[i]);
							continue;
						}
						if (log.isDebugEnabled())
							log.debug((new StringBuilder()).append("el metodo ").append(methodName).append(" con parametro de tipo ").append(parameters[0].getName()).append(" fu\351 descartado como setter utilizable por XSTREAM").toString());
					}

					List serializableProperties = new ArrayList();
					Iterator it = propertyMap.values().iterator();
					do {
						if (!it.hasNext())
							break;
						BeanProperty property = (BeanProperty)it.next();
						if (property.isReadable() || property.isWritable()) {
							if (log.isDebugEnabled())
								log.debug((new StringBuilder()).append("la propiedad ").append(property.getName()).append("  fu\351 incluida setter:").append(property.isWritable()).append(" getter:").append(property.isReadable()).append(" utilizable por XSTREAM").toString());
							serializableProperties.add(property);
						}
					} while (true);
					Collections.sort(serializableProperties, new BeanPropertyComparator());
					Map keyedByFieldName = new OrderRetainingMap();
					BeanReadOnlyProperty property;
					for (Iterator it2 = serializableProperties.iterator(); it2.hasNext(); keyedByFieldName.put(property.getName(), property)) {
						property = (BeanReadOnlyProperty)it2.next();
						if (!keyedByFieldName.containsKey(property.getName()))
							continue;
						BeanReadOnlyProperty propertyCached = (BeanReadOnlyProperty)keyedByFieldName.get(property.getName());
						if (Modifier.isAbstract(propertyCached.getType().getModifiers())) {
							property.setGetterMethod(propertyCached.getGetterMethod());
							continue;
						}
						if (Modifier.isAbstract(property.getType().getModifiers())) {
							propertyCached.setGetterMethod(property.getGetterMethod());
							property = propertyCached;
						}
					}

					keyedByPropertyNameCache.put(clsName, keyedByFieldName);
				}
			}
		return (Map)keyedByPropertyNameCache.get(clsName);
	}

}
