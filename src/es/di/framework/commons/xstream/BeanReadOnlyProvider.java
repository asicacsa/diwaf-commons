// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   BeanReadOnlyProvider.java

package es.di.framework.commons.xstream;

import com.thoughtworks.xstream.converters.javabean.BeanProperty;
import com.thoughtworks.xstream.converters.javabean.BeanProvider;
import com.thoughtworks.xstream.converters.reflection.ObjectAccessException;
import java.lang.reflect.*;
import java.util.Iterator;

// Referenced classes of package es.di.framework.commons.xstream:
//			PropertyReadOnlyDictionary

public class BeanReadOnlyProvider extends BeanProvider {

	protected PropertyReadOnlyDictionary propertyDictionary = null;
	protected static final Object NO_PARAMS[] = new Object[0];

	static interface Visitor {
		public abstract void visit(String s, Class class1, Object obj);
	}

	public BeanReadOnlyProvider() {
		propertyDictionary = new PropertyReadOnlyDictionary();
	}

	public boolean propertyWriteableDefinedInClass(String name, Class type) {
		BeanProperty bp = propertyDictionary.property(type, name);
		if (bp != null)
			return bp.isWritable();
		else
			throw new ObjectAccessException((new StringBuilder()).append("No such property ").append(type.getName()).append(".").append(name).append(" WRITEABLE!! ").toString());
	}

	public Object newInstance(Class type) {
		try {
			return getDefaultConstrutor(type).newInstance(NO_PARAMS);
		}
		catch (InstantiationException e) {
			throw new ObjectAccessException((new StringBuilder()).append("Cannot construct ").append(type.getName()).toString(), e);
		}
		catch (IllegalAccessException e) {
			throw new ObjectAccessException((new StringBuilder()).append("Cannot construct ").append(type.getName()).toString(), e);
		}
		catch (InvocationTargetException e) {
			if (e.getTargetException() instanceof RuntimeException)
				throw (RuntimeException)e.getTargetException();
			if (e.getTargetException() instanceof Error)
				throw (Error)e.getTargetException();
			else
				throw new ObjectAccessException((new StringBuilder()).append("Constructor for ").append(type.getName()).append(" threw an exception").toString(), e);
		}
	}

	public void visitSerializableProperties(Object object, Visitor visitor) {
		BeanProperty property;
		Object value;
		for (Iterator iterator = propertyDictionary.serializablePropertiesFor(object.getClass()); iterator.hasNext(); visitor.visit(property.getName(), property.getType(), value)) {
			property = (BeanProperty)iterator.next();
			value = null;
			try {
				value = property.get(object);
			}
			catch (IllegalArgumentException e) {
				throw new ObjectAccessException((new StringBuilder()).append("Could not get property ").append(property.getClass()).append(".").append(property.getName()).toString(), e);
			}
			catch (IllegalAccessException e) {
				throw new ObjectAccessException((new StringBuilder()).append("Could not get property ").append(property.getClass()).append(".").append(property.getName()).toString(), e);
			}
		}

	}

	public void writeProperty(Object object, String propertyName, Object value) {
		BeanProperty property = propertyDictionary.property(object.getClass(), propertyName);
		try {
			property.set(object, value);
		}
		catch (IllegalArgumentException e) {
			throw new ObjectAccessException((new StringBuilder()).append("Could not set property ").append(object.getClass()).append(".").append(property.getName()).toString(), e);
		}
		catch (IllegalAccessException e) {
			throw new ObjectAccessException((new StringBuilder()).append("Could not set property ").append(object.getClass()).append(".").append(property.getName()).toString(), e);
		}
	}

	public Class getPropertyType(Object object, String name) {
		return propertyDictionary.property(object.getClass(), name).getType();
	}

	public boolean propertyDefinedInClass(String name, Class type) {
		return propertyDictionary.property(type, name) != null;
	}

	public boolean canInstantiate(Class type) {
		return getDefaultConstrutor(type) != null;
	}

	protected Constructor getDefaultConstrutor(Class type) {
		Constructor constructors[] = type.getConstructors();
		for (int i = 0; i < constructors.length; i++) {
			Constructor c = constructors[i];
			if (c.getParameterTypes().length == 0 && Modifier.isPublic(c.getModifiers()))
				return c;
		}

		return null;
	}

}
