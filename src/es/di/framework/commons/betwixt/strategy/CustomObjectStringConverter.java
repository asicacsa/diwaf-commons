// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   CustomObjectStringConverter.java

package es.di.framework.commons.betwixt.strategy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.betwixt.Options;
import org.apache.commons.betwixt.expression.Context;
import org.apache.commons.betwixt.strategy.ConvertUtilsObjectStringConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CustomObjectStringConverter extends ConvertUtilsObjectStringConverter {

	private static final String DATE_FORMAT_PATTERN = "java.util.date.FormatPattern";
	private static final Log log = LogFactory.getLog(es.di.framework.commons.betwixt.strategy.CustomObjectStringConverter.class);
	private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss", new Locale("es", "ES"));
	/**API 29 Julio 2011 Pasamos a 3 decimales por defecto
	API 17 Agosto 2012 Por evitar tener que modificar todos los importes que puedan llegar desde la capa de servicios en la de presentacion para poder operar con ellos, serializamos a 2 posiciones redondeadas.
	private final DecimalFormat decimalformatter = new DecimalFormat("#.###");*/
	private final DecimalFormat decimalformatter = new DecimalFormat("#.##");

	public CustomObjectStringConverter() {}

	public String objectToString(Object object, Class type, String flavour, Context context) {
		if (object != null) {
			if (object instanceof Class)
				return ((Class)object).getName();
			if ((object instanceof Date) && isUtilDate(type)) {
				String pattern = null;
				Options options = context.getOptions();
				if (options != null) {
					pattern = options.getValue("java.util.date.FormatPattern");
					log.debug((new StringBuilder()).append("Cargamos el pattern de fechas : ").append(pattern).toString());
					log.debug((new StringBuilder()).append("Esta son las options que tenemos : ").append(options.getNames().length).toString());
					String opciones[] = options.getNames();
					for (int i = 0; i < opciones.length; i++) {
						log.debug((new StringBuilder()).append("Opcion nombre : ").append(opciones[i]).toString());
						log.debug((new StringBuilder()).append("Opcion valor : ").append(options.getValue(opciones[i])).toString());
					}

				} else {
					log.debug("Sin options");
				}
				if (pattern != null)
					formatter.applyPattern(pattern);
				return formatter.format((Date)object);
			}
			if ((object instanceof Float) && object != null) {
				try {
					/**
					API 29 Julio 2011 Utilizaremos 3 posiciones decimales a la hora de serializar
					return (new BigDecimal(Float.toString(((Float)object).floatValue()))).setScale(2, 6).toString();
					API 1 Agosto 2011 Este metodo deprecado no redondea como esperamos pasamos a usar el metodo recomendado
					return (new BigDecimal(Float.toString(((Float)object).floatValue()))).setScale(3, 6).toString();
					API 17 Agosto 2012 Por evitar tener que modificar todos los importes que puedan llegar desde la capa de servicios en la de presentacion para poder operar con ellos, serializamos a 2 posiciones redondeadas.
					return (new BigDecimal(Float.toString(((Float)object).floatValue()))).setScale(3, RoundingMode.HALF_EVEN).toString();*/
					return (new BigDecimal(Float.toString(((Float)object).floatValue()))).setScale(2, RoundingMode.HALF_EVEN).toString();
				}
				catch (Exception ex) {
					log.error((new StringBuilder()).append("error redondeando float!!! Aplicando redondeo por defecto. Excepcion: ").append(ex.getMessage()).toString());
				}
				/**API 29 Julio 2011 Utilizaremos 3 posiciones decimales a la hora de serializar
				float resultadoRedondeado = (float)Math.round(((Float)object).floatValue() * 100F) / 100F;
				API 17 Agosto 2012 Por evitar tener que modificar todos los importes que puedan llegar desde la capa de servicios en la de presentacion para poder operar con ellos, serializamos a 2 posiciones redondeadas.
				float resultadoRedondeado = (float)Math.round(((Float)object).floatValue() * 1000F) / 1000F;*/
				float resultadoRedondeado = (float)Math.round(((Float)object).floatValue() * 100F) / 100F;
				return (new Float(resultadoRedondeado)).toString();
			} else {
				return super.objectToString(object, type, flavour, context);
			}
		} else {
			return "";
		}
	}

	public Object stringToObject(String value, Class type, String flavour, Context context) {
		if (isUtilDate(type)) {
			try {
				return formatter.parse(value);
			}
			catch (ParseException ex) {
				handleException(ex);
			}
			return value;
		} else {
			return super.stringToObject(value, type, flavour, context);
		}
	}

	protected void handleException(Exception e) {
		throw new ConversionException((new StringBuilder()).append("String to object conversion failed: ").append(e.getMessage()).toString(), e);
	}

	private boolean isUtilDate(Class type) {
		return java.util.Date.class.isAssignableFrom(type) && !java.sql.Date.class.isAssignableFrom(type) && !java.sql.Time.class.isAssignableFrom(type) && !java.sql.Timestamp.class.isAssignableFrom(type);
	}

}