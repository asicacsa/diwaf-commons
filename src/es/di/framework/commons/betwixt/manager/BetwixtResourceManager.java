// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   BetwixtResourceManager.java

package es.di.framework.commons.betwixt.manager;

import java.io.IOException;
import java.util.*;
import net.sf.ehcache.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.*;
import org.xml.sax.InputSource;

public class BetwixtResourceManager
	implements ApplicationContextAware {

	private static final Log log = LogFactory.getLog(es.di.framework.commons.betwixt.manager.BetwixtResourceManager.class);
	private ResourceLoader resourceLoader = null;
	private Map betwixtFiles = null;
	private String betwixtCacheName = null;
	private CacheManager cacheManager = null;
	private boolean isCacheable = false;

	public BetwixtResourceManager() {
		resourceLoader = new DefaultResourceLoader();
		betwixtFiles = new HashMap();
		betwixtCacheName = null;
		cacheManager = null;
		isCacheable = true;
	}

	public String getBetwixtCacheName() {
		return betwixtCacheName;
	}

	public void setBetwixtCacheName(String betwixtCacheName) {
		this.betwixtCacheName = betwixtCacheName;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	public void setBetwixtFiles(Map betwixtFiles) {
		this.betwixtFiles = betwixtFiles;
	}

	public InputSource getDotBetwixt(String metodo) throws IOException {
		InputSource result;
		Resource aux;
		result = null;
		if (log.isInfoEnabled())
			log.info((new StringBuilder()).append("Call to ").append(getClass()).append(".getDotBetwixt(").append(metodo).append(")").toString());
		if (metodo == null)
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("Call to ").append(getClass()).append(".getDotBetwixt() with null argument.").toString());
		if (isCacheable)
			result = getCacheInputSource(metodo);
		if (result != null)
			if (log.isInfoEnabled())
				log.info((new StringBuilder()).append("Return from Cache : ").append(metodo).toString());
		if (!betwixtFiles.containsKey(metodo))
			if (log.isInfoEnabled())
				log.info((new StringBuilder()).append(" El Resource ").append(metodo).append(".betwixt no ha sido encontrado.").toString());		
		String pathClass = (String)betwixtFiles.get(metodo);
		if (log.isInfoEnabled())
			log.info((new StringBuilder()).append("Este es el fichero que vamos a obtener : ").append(pathClass).toString());
		if (log.isDebugEnabled())
			log.debug((new StringBuilder()).append("Intentamos recuperar el fichero ").append(pathClass).toString());
		aux = resourceLoader.getResource(pathClass);
		if (aux == null || !aux.exists()) {
			if (log.isInfoEnabled())
				log.info((new StringBuilder()).append(" El Resource ").append(metodo).append(".betwixt no ha sido encontrado.").toString());
			throw new IOException();
		}
		java.io.InputStream cacheStream = null;
		java.io.InputStream betwixtStream = null;
		try {
			cacheStream = aux.getInputStream();
			if (isCacheable) {
				String saveMe = IOUtils.toString(cacheStream);
				Element elemento = new Element(metodo, saveMe);
				getCacheManager().getCache(getBetwixtCacheName()).put(elemento);
				betwixtStream = aux.getInputStream();
				result = new InputSource(betwixtStream);
			} else {
				result = new InputSource(cacheStream);
			}
		} catch (IOException ioe) {
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append(" El archivo ").append(metodo).append(" no se encuentra en nuestra coleccion de datos.").toString(), ioe);
			throw ioe;
		}
		return result;
	}

	private InputSource getCacheInputSource(String metodo) throws CacheException, IOException {
		InputSource result = null;
		if (log.isInfoEnabled())
			log.info((new StringBuilder()).append("Call to getCacheInputSource with this parameter : ").append(metodo).toString());
		if (getCacheManager() != null) {
			Element element = null;
			Cache aux = cacheManager.getCache(getBetwixtCacheName());
			if (aux != null)
				try {
					element = aux.get(metodo);
					if (element != null) {
						Object objectresult = element.getObjectValue();
						if (objectresult != null && (objectresult instanceof String)) {
							java.io.InputStream input = IOUtils.toInputStream((String)objectresult);
							result = new InputSource(input);
						}
					}
				}
				catch (IllegalStateException ise) {
					if (log.isDebugEnabled())
						log.debug("The cache no is alive");
					throw ise;
				}
				catch (CacheException cex) {
					if (log.isDebugEnabled())
						log.debug((new StringBuilder()).append("CacheException : ").append(cex.getMessage()).toString());
					throw cex;
				}
				catch (ClassCastException cce) {
					if (log.isDebugEnabled())
						log.debug("ClassCastException. El elemento que tenemos en nuestra Cache no es un InputSource.", cce);
					throw cce;
				}
			else
			if (log.isInfoEnabled())
				log.info((new StringBuilder()).append("Doesnt have betwixt cache in the CacheManager : ").append(betwixtCacheName).toString());
		} else
		if (log.isInfoEnabled())
			log.info("Doesnt have CacheManager. ");
		return result;
	}

	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		if (isCacheable) {
			if (getCacheManager() == null)
				throw new IllegalArgumentException("CacheManager is required");
			if (getBetwixtCacheName() == null)
				throw new IllegalArgumentException("BetwixtCacheName is required");
		}
		if (betwixtFiles != null)
			refactorMapOfBetwixt();
	}

	private void refactorMapOfBetwixt() {
		String file_separator = System.getProperty("file.separator");
		java.util.Map.Entry element;
		String aux;
		for (Iterator iter = betwixtFiles.entrySet().iterator(); iter.hasNext(); element.setValue(aux)) {
			element = (java.util.Map.Entry)iter.next();
			aux = (String)element.getValue();
			aux = aux.replaceAll("\\.", (new StringBuilder()).append("\\").append(file_separator).toString());
			String key = (String)element.getKey();
			aux = (new StringBuilder()).append("classpath:").append(file_separator).append(aux).append(file_separator).append(key).append(".betwixt").toString();
		}

	}

	public boolean isCacheable() {
		return isCacheable;
	}

	public void setCacheable(boolean isCacheable) {
		this.isCacheable = isCacheable;
	}

}
