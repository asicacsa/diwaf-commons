// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   KeyValueObject.java

package es.di.framework.commons;

import org.apache.commons.collections.KeyValue;

public class KeyValueObject
	implements KeyValue {

	String key = null;
	String value = null;

	public KeyValueObject(String key_aux, String value_aux) {
		key = key_aux;
		value = value_aux;
	}

	public Object getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
