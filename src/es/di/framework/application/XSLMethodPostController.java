// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   XSLMethodPostController.java

package es.di.framework.application;

import es.di.framework.service.controller.ISOAServiceController;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.*;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.xml.sax.InputSource;

// Referenced classes of package es.di.framework.application:
//			CommandParameter

public class XSLMethodPostController extends AbstractCommandController
	implements InitializingBean {

	ISOAServiceController service = null;
	private String inputxsl = null;
	private String outputxsl = null;
	private String test = null;
	protected static final Log logger = LogFactory.getLog(es.di.framework.application.XSLMethodPostController.class);
	private CustomDateEditor dateEditor = null;

	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder requestDB) throws Exception {
		super.initBinder(request, requestDB);
		requestDB.registerCustomEditor(java.util.Date.class, dateEditor);
	}

	public XSLMethodPostController() {
		service = null;
		inputxsl = null;
		outputxsl = null;
		test = null;
		dateEditor = new CustomDateEditor(new SimpleDateFormat("yyyy/MM/dd"), true);
		setCommandClass(es.di.framework.application.CommandParameter.class);
	}

	public void setService(ISOAServiceController service) {
		this.service = service;
	}

	private void writeTestFile(String contens, String file) throws Exception {
		if (logger.isDebugEnabled())
			logger.debug((new StringBuilder()).append("Escribimos el fichero de test de trasnformacion : ").append(file).toString());
		File file_w = new File(file);
		FileWriter writer = new FileWriter(file_w);
		writer.write(contens);
		writer.flush();
		writer.close();
	}

	protected ModelAndView handle(HttpServletRequest request, HttpServletResponse response, Object command, BindException arg3) throws Exception {
		CommandParameter cp = (CommandParameter)command;
		response.setContentType("text/xml");
		if (inputxsl != null) {
			if (test != null && test.equals("true"))
				writeTestFile(cp.getXml(), (new StringBuilder()).append(System.getProperty("user.home")).append(File.separator).append("xmlInvocationInput.xml").toString());
			ByteArrayOutputStream salida = new ByteArrayOutputStream();
			StreamResult resultado_input = new StreamResult(salida);
			transform(getSourceXML(cp.getXml()), getSourceXSL(inputxsl), resultado_input);
			cp.setXml(salida.toString("UTF-8"));
			if (test != null && test.equals("true"))
				writeTestFile(cp.getXml(), (new StringBuilder()).append(System.getProperty("user.home")).append(File.separator).append("xmlResultInput.xml").toString());
		}
		String respuesta = null;
		try {
			respuesta = service.process(cp.getServicio(), cp.getXml());
		}
		catch (Exception ex) {
			if (logger.isDebugEnabled())
				logger.debug((new StringBuilder()).append("Exception llamando al Service : ").append(cp.getServicio()).toString(), ex);
			throw ex;
		}
		String nstr = null;
		if (outputxsl != null) {
			if (test != null && test.equals("true"))
				writeTestFile(respuesta, (new StringBuilder()).append(System.getProperty("user.home")).append(File.separator).append("xmlResultService.xml").toString());
			ByteArrayOutputStream salida = new ByteArrayOutputStream();
			Result resultado_output = new StreamResult(salida);
			transform(getSourceXML(cp.getXml()), getSourceXSL(outputxsl), resultado_output);
			nstr = salida.toString("ISO-8859-1");
			if (test != null && test.equals("true"))
				writeTestFile(nstr, (new StringBuilder()).append(System.getProperty("user.home")).append(File.separator).append("xmlResultOutPut.xml").toString());
		} else {
			byte p[] = respuesta.getBytes("UTF-8");
			nstr = new String(p, 0, p.length, "ISO-8859-1");
			if (test != null && test.equals("true"))
				writeTestFile(nstr, (new StringBuilder()).append(System.getProperty("user.home")).append(File.separator).append("xmlResultService.xml").toString());
		}
		response.getOutputStream().println(nstr);
		return null;
	}

	public void setInputxsl(String inputxsl) {
		this.inputxsl = inputxsl;
	}

	public void setOutputxsl(String outputxsl) {
		this.outputxsl = outputxsl;
	}

	public void afterPropertiesSet() throws Exception {
		if (service == null)
			throw new IllegalArgumentException("Service is required");
		else
			return;
	}

	private Source getSourceXML(String source) throws Exception {
		Source xml_entrada = new StreamSource(new StringReader(source));
		return xml_entrada;
	}

	private Source getSourceXSL(String file) throws Exception {
		ServletContextResource file_aux = new ServletContextResource(getServletContext(), file);
		Source source = new SAXSource(new InputSource(file_aux.getInputStream()));
		return source;
	}

	protected void transform(Source source, Source xsl, Result output) throws Exception {
		TransformerFactory trasnfor = TransformerFactory.newInstance();
		Transformer transformer = trasnfor.newTransformer(xsl);
		try {
			transformer.transform(source, output);
		}
		catch (Exception e) {
			if (logger.isDebugEnabled())
				logger.debug("No se ha podido realizar la trasnformacion.", e);
			throw e;
		}
	}

	public void setTest(String test) {
		this.test = test;
	}

}
