// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   CommandParameter.java

package es.di.framework.application;


public class CommandParameter {

	private String xml = null;
	private String servicio = null;

	public CommandParameter() {
		xml = null;
		servicio = null;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}
}
