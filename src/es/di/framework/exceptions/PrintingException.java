// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   PrintingException.java

package es.di.framework.exceptions;

import java.io.Serializable;

public class PrintingException extends Exception
	implements Serializable {

	public PrintingException() {
	}

	public PrintingException(String arg0) {
		super(arg0);
	}

	public PrintingException(Throwable arg0) {
		super(arg0);
	}

	public PrintingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
