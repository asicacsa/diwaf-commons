// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   BusinessRuleException.java

package es.di.framework.exceptions;


public class BusinessRuleException extends Exception {

	public static final int SEVERITY_WARNING = 0;
	public static final int SEVERITY_ERROR = 1;
	public static final int DEFAULT_SEVERITY_ERROR = 0;
	private int severity = 0;

	public BusinessRuleException() {
		severity = 0;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public BusinessRuleException(String message) {
		super(message);
		severity = 0;
	}

	public BusinessRuleException(Throwable cause) {
		super(cause);
		severity = 0;
	}

	public BusinessRuleException(String message, Throwable cause) {
		super(message, cause);
		severity = 0;
	}

	public BusinessRuleException(String message, int severity) {
		super(message);
		this.severity = 0;
		setSeverity(severity);
	}

	public BusinessRuleException(Throwable cause, int severity) {
		super(cause);
		this.severity = 0;
		setSeverity(severity);
	}

	public BusinessRuleException(String message, Throwable cause, int severity) {
		super(message, cause);
		this.severity = 0;
		setSeverity(severity);
	}
}
