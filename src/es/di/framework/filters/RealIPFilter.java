// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   RealIPFilter.java

package es.di.framework.filters;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RealIPFilter
	implements Filter {

	public RealIPFilter() {
	}

	public void init(FilterConfig filterconfig) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (request instanceof HttpServletRequest) {
			HttpSession sesion = ((HttpServletRequest)request).getSession(false);
			if (sesion == null) {
				sesion = ((HttpServletRequest)request).getSession(true);
				sesion.setAttribute("realip", request.getRemoteAddr());
			}
		}
		chain.doFilter(request, response);
	}

	public void destroy() {
	}
}
