// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   SOAServiceInvoker.java

package es.di.framework.service.controller;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import es.di.framework.commons.betwixt.manager.BetwixtResourceManager;
import es.di.framework.commons.betwixt.strategy.CustomObjectStringConverter;
import es.di.framework.commons.xstream.JavaBeanReadOnlyConverter;
import es.di.framework.framework.ServiceMap;
import es.di.framework.service.ISecureService;
import es.di.framework.service.IService;
import java.beans.IntrospectionException;
import java.io.*;
import java.lang.reflect.Method;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.betwixt.*;
import org.apache.commons.betwixt.io.BeanWriter;
//import org.apache.commons.betwixt.io.CyclicReferenceException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SOAServiceInvoker
	implements IService, ISecureService, Controller, ApplicationContextAware, InitializingBean {

	protected static final Log log = LogFactory.getLog(es.di.framework.service.controller.SOAServiceInvoker.class);
	private static String ID_REFERENCES = "ID_REFERENCES";
	private static String NO_REFERENCES = "NO_REFERENCES";
	private String serviceName = null;
	String methodName = null;
	Properties alias = null;
	Object secureContext = null;
	String referenceXStream = null;
	boolean useBetwixt = false;
	String securityCod = null;
	boolean enableLimitResponseBetwixt = false;
	int maxTagResponseBetwixt = 0;
	Method method = null;
	private ApplicationContext context = null;
	private String betwixtManagerName = null;

	public SOAServiceInvoker() {
		useBetwixt = false;
		enableLimitResponseBetwixt = false;
		maxTagResponseBetwixt = 10000;
	}

	public String execute(String inputxml) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug((new StringBuilder()).append("XML que recibe el metodo SOAServiceInvoker.execute :\n").append(inputxml).toString());
			log.debug((new StringBuilder()).append("Vamos a llamar al metodo : ").append(methodName).append(" desde el SOAServiceInvoker.execute :").toString());
		}
		XStream xstream = new XStream();
		if (serviceName == null || serviceName.equals(""))
			log.warn((new StringBuilder()).append("Hemos configurado mal este bean : ").append(toString()).toString());
		Object service = context.getBean(serviceName);
		xstream.alias("servicio", es.di.framework.framework.ServiceMap.class);
		JavaBeanReadOnlyConverter jbc = new JavaBeanReadOnlyConverter(xstream.getMapper(), /*getClassMapper(),*/ "class");
		xstream.registerConverter(jbc, -20);
		DateConverter dc = new DateConverter("dd/MM/yyyy HH:mm:ss", new String[] {
			"dd/MM/yyyy", "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss.S a", "yyyy-MM-dd HH:mm:ssz", "yyyy-MM-dd HH:mm:ssa", "dd/MM/yyyy-HH:mm:ss"
		});
		xstream.registerConverter(dc);
		String name;
		for (Enumeration e = alias.propertyNames(); e.hasMoreElements(); xstream.alias(name, Class.forName(alias.getProperty(name))))
			name = (String)e.nextElement();

		ServiceMap bag = null;
		try {
			bag = (ServiceMap)xstream.fromXML(inputxml);
		}
		catch (ClassCastException e) {
			service = null;
			if (log.isWarnEnabled())
				log.warn((new StringBuilder()).append("Exception in the SOAServiceInvoker. Calling this services : ").append(methodName).append(" with this parameters : ").append(inputxml).toString(), e);
			throw new IllegalArgumentException("El xml de petici\357\277\275n no es valido, la raiz debe llamarse <servicio> y los parametros deben estar dentro de elementos <parametro>");
		}
		catch (Exception ex) {
			service = null;
			if (log.isWarnEnabled())
				log.warn((new StringBuilder()).append("Exception in the SOAServiceInvoker. Calling this services : ").append(methodName).append(" with this parameters : ").append(inputxml).toString(), ex);
			throw ex;
		}
		Vector parameters = bag.getParametro();
		if (log.isDebugEnabled())
			log.debug((new StringBuilder()).append("Parametros para el servicio (").append(methodName).append(") llamado :").append(parameters.toString()).toString());
		Object objresult;
		if (parameters != null && parameters.size() > 0) {
			Class parameterTypes[] = new Class[parameters.size()];
			Object params[] = new Object[parameters.size()];
			int i = 0;
			for (Iterator iter = parameters.iterator(); iter.hasNext();) {
				Object param = iter.next();
				params[i] = param;
				parameterTypes[i] = params[i].getClass();
				i++;
			}

			method = MethodUtils.getMatchingAccessibleMethod(service.getClass(), methodName, parameterTypes);
			if (method == null)
				log.error((new StringBuilder()).append("No se ha encontrado el metodo ").append(methodName).append(" con parametros ").append(parameterTypes).append(" de la clase ").append(service.getClass().toString()).toString());
			objresult = method.invoke(service, params);
		} else {
			method = service.getClass().getMethod(methodName, new Class[0]);
			if (method == null)
				log.error((new StringBuilder()).append("No se ha encontrado el metodo ").append(methodName).append(" sin parametros de la clase ").append(service.getClass().toString()).toString());
			objresult = method.invoke(service, new Object[0]);
		}
		if (log.isDebugEnabled())
			log.debug((new StringBuilder()).append("Utilizando betwixt para mostrar el resultado: ").append(useBetwixt).toString());
		String strRetorno;
		if (!useBetwixt) {
			xstream.setMode(getXStreamMode());
			strRetorno = xstream.toXML(objresult);
			objresult = null;
			return strRetorno;
		}
		try {
			strRetorno = objresult == null ? null : getBetwixtXML(objresult);
		}
		catch (Exception ex) {
			log.error((new StringBuilder()).append(" ## Exception in the SOAServiceInvoker. Serialazing the result of the call to this services : ").append(methodName).append(" with this parameters : ").append(inputxml).toString());
			throw ex;
		}
		objresult = null;
		service = null;
		return strRetorno;
	}

	private String getBetwixtXML(Object ob) throws IOException {
		StringWriter sw;
		BeanWriter bw;
		XMLIntrospector xi;
		sw = new StringWriter();
		bw = new BeanWriter(sw);
		BindingConfiguration bc = bw.getBindingConfiguration();
		org.apache.commons.betwixt.strategy.ObjectStringConverter converter = new CustomObjectStringConverter();
		bc.setObjectStringConverter(converter);
		xi = bw.getXMLIntrospector();
		if (enableLimitResponseBetwixt) {
			bw.enablePrettyPrint();
			bw.enableLimitResponse();
			bw.setMaxTagResponse(maxTagResponseBetwixt);
		}
		bc.setMapIDs(false);
		xi.getConfiguration().setWrapCollectionsInElement(true);
		BetwixtResourceManager betwixtManager = (BetwixtResourceManager)context.getBean(betwixtManagerName);
		InputSource source = betwixtManager.getDotBetwixt(methodName);
		if (source != null)
			try {
				xi.register(source);
			} catch (IntrospectionException e) {
				log.error("BeanWriter", e);
			} catch (SAXException e) {
				log.error("BeanWriter", e);
			}
		try {
			bw.write(ob);
		} catch (IntrospectionException e) {
			log.error("BeanWriter", e);
		} catch (SAXException e) {
			log.error("BeanWriter", e);
		}
		/*BetwixtResourceManager betwixtManager = null;
		break MISSING_BLOCK_LABEL_225;
		SAXException saxe;
		saxe;
		log.error("BeanWriter", saxe);
		BetwixtResourceManager betwixtManager = null;
		break MISSING_BLOCK_LABEL_225;
		IntrospectionException ie;
		ie;
		log.error("BeanWriter", ie);
		BetwixtResourceManager betwixtManager = null;
		break MISSING_BLOCK_LABEL_225;
		CyclicReferenceException cre;
		cre;
		log.error("CyclicReferenceException", cre);
		sw.write("ERROR de referencia ciclica serializando XML");
		BetwixtResourceManager betwixtManager = null;
		break MISSING_BLOCK_LABEL_225;
		ClassCastException cce;
		cce;
		log.error("El betwixtManagerName no es un bean del tipo requerido.");
		throw cce;
		Exception exception;
		exception;
		BetwixtResourceManager betwixtManager = null;
		throw exception;*/
		bw.flush();
		bw.close();
		return sw.toString();
	}

	/**
	 * @deprecated Method getDotBetwixt is deprecated
	 */

	private InputSource getDotBetwixt(Object service, String metodo) {
		if (log.isDebugEnabled()) {
			log.debug((new StringBuilder()).append("Esta es la clase en la que vamos a buscar el archivo Betwixt : ").append(service.getClass().getPackage().toString()).toString());
			log.debug((new StringBuilder()).append("Esta es el archivo Betwixt : ").append(metodo).append(".betwixt").toString());
		}
		java.io.InputStream is = null;
		Resource res = null;
		res = context.getResource((new StringBuilder()).append("classpath:").append(metodo).append(".betwixt").toString());
		if (res == null || !res.exists()) {
			if (log.isInfoEnabled())
				log.info((new StringBuilder()).append("No se puede recuperar el resource : ").append(res.toString()).append(" del archivo : ").append(metodo).append(".betwixt ").toString());
			return null;
		}
		try {
			is = res.getInputStream();
		}
		catch (IOException ioe) {
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("El archivo : classpath :").append(metodo).append(".betwixt no se puede abrir.").toString(), ioe);
			return null;
		}
		return new InputSource(is);
	}

	public void setAlias(Properties alias) {
		this.alias = alias;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @deprecated Method setService is deprecated
	 */

	public void setService(Object obj) {
	}

	public void setSecurityContext(Object sc) {
		secureContext = sc;
	}

	public String getReferenceXStream() {
		return referenceXStream;
	}

	public void setReferenceXStream(String referenceXStream) {
		this.referenceXStream = referenceXStream;
	}

	public int getXStreamMode() {
		int resultado = 1003;
		if (getReferenceXStream() != null)
			if (getReferenceXStream().equals(ID_REFERENCES))
				resultado = 1002;
			else
			if (getReferenceXStream().equals(NO_REFERENCES))
				resultado = 1001;
		return resultado;
	}

	public ModelAndView handleRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Writer w = res.getWriter();
		w.write("<html><body>");
		String exe = req.getParameter("exe");
		String xml = req.getParameter("xml");
		if (xml == null || xml.length() == 0)
			xml = createXMLExample(xml);
		w.write("<form METHOD='POST'>");
		w.write("<textarea name='xml' cols='70' rows='15'>");
		w.write(res.encodeURL(xml));
		w.write("</textarea>");
		w.write("<input type='hidden' name='exe' value='1'></input>");
		w.write("<input type='submit'></input>");
		w.write("</form>");
		w.write("<br/><hr/>");
		if (exe != null && exe.length() > 0) {
			w.write("<textarea name='xml' cols='70' rows='30'>");
			String result = execute(xml);
			if (result != null)
				w.write(res.encodeURL(result));
			else
				w.write(res.encodeURL("<null>Este servicio no retorno nada. Etiqueta Informativa.</null>"));
			w.write("</textarea>");
		}
		w.write("</body></html>");
		return null;
	}

	private String createXMLExample(String xml) throws Exception {
		XStream xstream = new XStream();
		String name;
		for (Enumeration e = alias.propertyNames(); e.hasMoreElements(); xstream.alias(name, Class.forName(alias.getProperty(name))))
			name = (String)e.nextElement();

		StringBuffer result = new StringBuffer("<servicio>\n");
		Object service = context.getBean(serviceName);
		Method m = null;
		Method ms[] = service.getClass().getMethods();
		for (int i = 0; i < ms.length; i++)
			if (ms[i].getName().equals(methodName))
				m = ms[i];

		Class types[] = m.getParameterTypes();
		if (types.length > 0) {
			result.append("<parametro>\n");
			for (int i = 0; i < types.length; i++) {
				try {
					result.append(xstream.toXML(types[i].newInstance()));
					continue;
				}
				catch (InstantiationException ex) {
					result.append("<").append(types[i].getName()).append(">");
				}
				result.append("</").append(types[i].getName()).append(">\n");
			}

			result.append("</parametro>\n");
		}
		result.append("</servicio>");
		service = null;
		return result.toString();
	}

	public void setEnableLimitResponseBetwixt(boolean enableLimitResponseBetwixt) {
		setUseBetwixt(true);
		this.enableLimitResponseBetwixt = enableLimitResponseBetwixt;
	}

	public void setMaxTagResponseBetwixt(int maxTagResponseBetwixt) {
		this.maxTagResponseBetwixt = maxTagResponseBetwixt;
	}

	public void setUseBetwixt(boolean useBetwixt) {
		this.useBetwixt = useBetwixt;
	}

	public void setSecurityCod(String securityCod) {
		this.securityCod = securityCod;
	}

	public String getSecurityCod() {
		return securityCod;
	}

	public ApplicationContext getApplicationContext() {
		return context;
	}

	public void setApplicationContext(ApplicationContext context) {
		this.context = context;
	}

	/**
	 * @deprecated Method setBetwixtManager is deprecated
	 */

	public void setBetwixtManager(BetwixtResourceManager betwixtresourcemanager) {
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public void setBetwixtManagerName(String betwixtManagerName) {
		this.betwixtManagerName = betwixtManagerName;
	}

	public void afterPropertiesSet() throws Exception {
		if (useBetwixt) {
			Assert.notNull(betwixtManagerName, "betwixtManagerName is required if you will use Betwixt");
			Object aux = context.getBean(betwixtManagerName);
			Assert.notNull(aux, (new StringBuilder()).append("Does not exist Bean with betwixtManagerName ").append(betwixtManagerName).toString());
			Assert.isInstanceOf(es.di.framework.commons.betwixt.manager.BetwixtResourceManager.class, aux, "betwixtManagerName must be the name of a Bean instanceOf BetwixtResourceManager");
			aux = null;
		}
	}

	public String getMethodName() {
		return methodName;
	}

	public String getServiceName() {
		return serviceName;
	}

}
