// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   SOAServiceController.java

package es.di.framework.service.controller;

import es.di.framework.service.ISecureService;
import es.di.framework.service.IService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

// Referenced classes of package es.di.framework.service.controller:
//			ISOAServiceController

public class SOAServiceController
	implements ISOAServiceController, ApplicationContextAware, ISecureService {

	private ApplicationContext context = null;
	private Object secureContext = null;

	public SOAServiceController() {
	}

	public String process(String serviceName, String inputxml) throws Exception {
		IService service = (IService)context.getBean(serviceName);
		applySecurity(service);
		String result = service.execute(inputxml);
		return result;
	}

	private void applySecurity(IService service) throws Exception {
		if (service instanceof ISecureService)
			((ISecureService)service).setSecurityContext(secureContext);
		secureContext = null;
	}

	public void setApplicationContext(ApplicationContext context) {
		this.context = context;
	}

	public void setSecurityContext(Object sc) {
		secureContext = sc;
	}
}
