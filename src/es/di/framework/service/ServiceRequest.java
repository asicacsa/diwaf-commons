// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   ServiceRequest.java

package es.di.framework.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ServiceRequest
	implements ApplicationContextAware {

	String serviceName = null;
	List serviceInputs = null;
	transient ApplicationContext context = null;
	transient String soaServiceName = null;

	public ServiceRequest() {
	}

	public static void main(String args1[]) {
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String string) {
		serviceName = string;
	}

	public List getServiceInputs() {
		return serviceInputs;
	}

	public void setServiceInputs(List list) {
		serviceInputs = list;
	}

	public void addRequestInput(String key, String value) {
		if (serviceInputs == null)
			serviceInputs = new ArrayList();
		serviceInputs.add(value);
	}

	public void addRequestInput(String key, Object value) {
		if (serviceInputs == null)
			serviceInputs = new ArrayList();
		serviceInputs.add(value);
	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;
	}

	public void setSoaServiceName(String soaServiceName) {
		this.soaServiceName = soaServiceName;
	}
}
