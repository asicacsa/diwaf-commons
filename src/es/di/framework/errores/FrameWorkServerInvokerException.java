// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   FrameWorkServerInvokerException.java

package es.di.framework.errores;

import java.io.IOException;

public class FrameWorkServerInvokerException extends IOException {

	private static final long serialVersionUID = 0x9f44a5d7cee29268L;
	private String _message = null;
	private int _severity = 0;
	private Integer _statusCode = null;
	public static final int SEVERITY_WARNING = 0;
	public static final int SEVERITY_ERROR = 1;
	public static final int SEVERITY_FATAL = 2;

	public FrameWorkServerInvokerException(Exception e, int severity, Integer statusCode) {
		_message = e.getMessage();
		_severity = severity;
		_statusCode = statusCode;
	}

	public FrameWorkServerInvokerException(Exception e, int severity) {
		_message = e.getMessage();
		_severity = severity;
	}

	public FrameWorkServerInvokerException(Exception e) {
		_message = e.getMessage();
		_severity = 1;
	}

	public FrameWorkServerInvokerException() {
	}

	public int getSeverity() {
		return _severity;
	}

	public void setSeverity(int severity) {
		_severity = severity;
	}

	public String getMessage() {
		return (new StringBuilder()).append("<error><descripion>").append(_message).append("</descripion><severity>").append(_severity).append("</severity></error>").toString();
	}

	public void setMessage(String message) {
		_message = message;
	}

	public Integer get_statusCode() {
		return _statusCode;
	}

	public void set_statusCode(Integer code) {
		_statusCode = code;
	}
}
