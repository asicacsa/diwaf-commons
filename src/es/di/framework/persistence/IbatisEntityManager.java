// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   IbatisEntityManager.java

package es.di.framework.persistence;

import com.ibatis.sqlmap.client.SqlMapClient;
import java.lang.reflect.Field;
import java.sql.SQLException;
import javax.persistence.*;

// Referenced classes of package es.di.framework.persistence:
//			IbatisQuery

public class IbatisEntityManager
	implements EntityManager {

	private SqlMapClient sqlMapClient = null;
	private static int STR_INSERT = 0;
	private static int STR_FINDBYID = 1;
	private static int STR_UPDATE = 2;
	private static int STR_DELETE = 3;
	private static String prefijos[] = {
		"insert", "findById", "update", "delete"
	};

	public IbatisEntityManager() {
	}

	public void setSqlMapClient(SqlMapClient sqlMapClient) {
		this.sqlMapClient = sqlMapClient;
	}

	private static String getNombreClase(Class clase) {
		String nombre = clase.getName();
		int ultimoPunto = nombre.lastIndexOf(".") + 1;
		nombre = nombre.substring(ultimoPunto);
		return nombre;
	}

	public void persist(Object instanciaNueva) throws SQLException {
		if (instanciaNueva != null) {
			String methodName = (new StringBuilder()).append(prefijos[STR_INSERT]).append(getNombreClase(instanciaNueva.getClass())).toString();
			sqlMapClient.insert(methodName, instanciaNueva);
		}
	}

	public Object merge(Object instancia) throws SQLException {
		if (instancia != null) {
			String methodName = (new StringBuilder()).append(prefijos[STR_UPDATE]).append(getNombreClase(instancia.getClass())).toString();
			sqlMapClient.update(methodName, instancia);
		}
		return instancia;
	}

	public void remove(Object instancia) throws SQLException {
		if (instancia != null) {
			String methodName = (new StringBuilder()).append(prefijos[STR_DELETE]).append(getNombreClase(instancia.getClass())).toString();
			sqlMapClient.delete(methodName, instancia);
		}
	}

	public Object find(Class clase, Object primaryKey) throws SQLException {
		Object objeto = null;
		if (primaryKey != null) {
			String methodName = (new StringBuilder()).append(prefijos[STR_FINDBYID]).append(getNombreClase(clase)).toString();
			objeto = sqlMapClient.queryForObject(methodName, primaryKey);
		}
		return objeto;
	}

	public void flush() {
	}

	public Query createQuery(String queryEJB) {
		throw new UnsupportedOperationException();
	}

	public Query createNamedQuery(String nombre) {
		return new IbatisQuery(nombre, sqlMapClient);
	}

	public Query createNativeQuery(String sql) {
		throw new UnsupportedOperationException();
	}

	public Query createNativeQuery(String nombre, Class resultClass) {
		return null;
	}

	public Query createNativeQuery(String nombre, String resultSetMapping) {
		return null;
	}

	public void refresh(Object instancia) throws NoSuchFieldException, SQLException {
		Object id = getClavePrimaria(instancia);
		String methodName = (new StringBuilder()).append(prefijos[STR_FINDBYID]).append(getNombreClase(instancia.getClass())).toString();
		sqlMapClient.queryForObject(methodName, id, instancia);
	}

	private Object getClavePrimaria(Object instancia) throws NoSuchFieldException {
		String nombreId = (new StringBuilder()).append("id").append(getNombreClase(instancia.getClass())).toString();
		Field id = instancia.getClass().getField(nombreId);
		Object valorId = null;
		try {
			valorId = id.get(instancia);
		}
		catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return valorId;
	}

	public boolean contains(Object instancia) {
		return true;
	}

	public void setFlushMode(FlushModeType flushmodetype1) {
	}

}
