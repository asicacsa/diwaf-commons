// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   IbatisQuery.java

package es.di.framework.persistence;

import com.ibatis.sqlmap.client.SqlMapClient;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.*;
import javax.persistence.*;

public class IbatisQuery
	implements Query {

	private String nombreQuery = null;
	private SqlMapClient sqlClient = null;
	private Map parametro = null;

	public IbatisQuery(String nombre, SqlMapClient sqlMapClient) {
		parametro = new HashMap();
		nombreQuery = nombre;
		sqlClient = sqlMapClient;
	}

	private Object getParametroQuery() {
		if (parametro.values().size() > 1)
			return parametro;
		if (parametro.values().size() > 0)
			return parametro.values().iterator().next();
		else
			return null;
	}

	public List getResultList() throws SQLException {
		return sqlClient.queryForList(nombreQuery, getParametroQuery());
	}

	public Object getSingleResult() throws SQLException {
		return sqlClient.queryForObject(nombreQuery, getParametroQuery());
	}

	public int executeUpdate() throws SQLException {
		int numRegs = sqlClient.update(nombreQuery, getParametroQuery());
		return numRegs;
	}

	public Query setMaxResults(int i) {
		throw new UnsupportedOperationException();
	}

	public Query setFirstResult(int i) {
		throw new UnsupportedOperationException();
	}

	public Query setHint(String s, Object obj) {
		throw new UnsupportedOperationException();
	}

	public Query setParameter(String s, Object obj) {
		if (s == null) {
			parametro.put((new StringBuilder()).append("param").append(parametro.values().size()).toString(), obj);
			return this;
		}
		if (s.length() == 0) {
			throw new IllegalArgumentException("IbatisQuery:Error pasando el parametro");
		} else {
			parametro.put(s, obj);
			return this;
		}
	}

	public Query setParameter(String s, Date date, TemporalType temporaltype) {
		throw new UnsupportedOperationException();
	}

	public Query setParameter(String s, Calendar calendar, TemporalType temporaltype) {
		throw new UnsupportedOperationException();
	}

	public Query setParameter(int i, Object obj) {
		parametro.put((new StringBuilder()).append("param").append(i).toString(), obj);
		return this;
	}

	public Query setParameter(int i, Date date, TemporalType temporaltype) {
		throw new UnsupportedOperationException();
	}

	public Query setParameter(int i, Calendar calendar, TemporalType temporaltype) {
		throw new UnsupportedOperationException();
	}

	public Query setFlushMode(FlushModeType flushmodetype) {
		return this;
	}

	public static void main(String args[]) {
		IbatisQuery qActualizarFechaFin = new IbatisQuery(null, null);
		qActualizarFechaFin.setParameter(((String) (null)), new Integer(3));
		System.out.println(qActualizarFechaFin.getParametroQuery());
	}
}
