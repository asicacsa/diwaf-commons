// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   TestSOAServiceInvoker.java

package es.di.framework.test;

import es.di.framework.service.controller.ISOAServiceController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TestSOAServiceInvoker
	implements ISOAServiceController {

	protected static final Log logger = LogFactory.getLog(es.di.framework.test.TestSOAServiceInvoker.class);

	public TestSOAServiceInvoker() {
	}

	public String process(String serviceName, String inputxml) throws Exception {
		if (logger.isInfoEnabled()) {
			logger.info("Estamos en TestSOAServiceInvoker : ");
			logger.info((new StringBuilder()).append("XML de entrada : ").append(inputxml).toString());
			logger.info((new StringBuilder()).append("Servicio llamado : ").append(serviceName).toString());
		}
		return inputxml;
	}

}
