// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   SimpleXSLTransformerController.java

package es.di.framework.controllers;

import java.io.FileOutputStream;
import java.io.FileReader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.*;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.InputSource;

// Referenced classes of package es.di.framework.controllers:
//			OrquestController

public abstract class SimpleXSLTransformerController extends OrquestController {

	public SimpleXSLTransformerController() {
	}

	protected void transform(Source source, Source xsl, Result output) throws Exception {
		TransformerFactory trasnfor = TransformerFactory.newInstance();
		Transformer transformer = trasnfor.newTransformer(xsl);
		try {
			transformer.transform(source, output);
		}
		catch (Exception e) {
			throw e;
		}
	}

	protected Source getXMLSource(String XMLFileName) throws Exception {
		Source source = new SAXSource(new InputSource(new FileReader(XMLFileName)));
		return source;
	}

	protected Source getXSLSource(String XSLFileName) throws Exception {
		Source style = new SAXSource(new InputSource(new FileReader(XSLFileName)));
		return style;
	}

	protected void execute(String filename, String xslfilename, String outputFile) throws Exception {
		Source xsl = getXSLSource(xslfilename);
		Source xml = getXMLSource(filename);
		StreamResult result = new StreamResult(new FileOutputStream(outputFile));
		transform(xml, xsl, result);
	}

	public abstract ModelAndView handleRequest(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse) throws Exception;
}
