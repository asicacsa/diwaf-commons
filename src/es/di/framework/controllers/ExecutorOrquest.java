// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   ExecutorOrquest.java

package es.di.framework.controllers;

import java.io.PrintStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class ExecutorOrquest
	implements Controller {

	private String inputParam = null;
	private String outputParam = null;

	public ExecutorOrquest() {
		inputParam = null;
		outputParam = null;
	}

	public ModelAndView handleRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		ModelAndView resul = null;
		Object input = getInput(arg0);
		System.out.println("Esto es lo que nos llega");
		if (input != null)
			System.out.println((new StringBuilder()).append("Esto es lo que nos llega!!! : ").append(input.toString()).toString());
		setOutput(arg0, new String("Hola Mundo!!!"));
		return resul;
	}

	public String getInputParam() {
		return inputParam;
	}

	public void setInputParam(String inputParam) {
		this.inputParam = inputParam;
	}

	public String getOutputParam() {
		return outputParam;
	}

	public void setOutputParam(String outputParam) {
		this.outputParam = outputParam;
	}

	protected Object getInput(HttpServletRequest request) throws Exception {
		Object resul = null;
		if (inputParam != null)
			resul = request.getAttribute(inputParam);
		return resul;
	}

	protected void setOutput(HttpServletRequest request, Object output) throws Exception {
		if (outputParam != null)
			request.setAttribute(outputParam, output);
	}
}
