// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   OrquestController.java

package es.di.framework.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public abstract class OrquestController
	implements Controller, BeanNameAware, InitializingBean, ApplicationContextAware {

	private Controller next = null;
	private Controller serviceExecutor = null;
	private String beanName = null;
	private ApplicationContext context = null;
	protected static final Log logger = LogFactory.getLog(es.di.framework.controllers.OrquestController.class);

	public OrquestController() {
		next = null;
		serviceExecutor = null;
		beanName = null;
		context = null;
	}

	public abstract ModelAndView handleRequest(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse) throws Exception;

	protected ModelAndView callToServiceExecutor(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		ModelAndView resul = null;
		if (serviceExecutor != null)
			resul = serviceExecutor.handleRequest(arg0, arg1);
		return resul;
	}

	protected ModelAndView callToNext(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		ModelAndView resul = null;
		if (next != null)
			resul = next.handleRequest(arg0, arg1);
		return resul;
	}

	public Controller getNext() {
		return next;
	}

	public void setNext(Controller next) {
		this.next = next;
	}

	public Controller getServiceExecutor() {
		return serviceExecutor;
	}

	public void setServiceExecutor(Controller serviceExecutor) {
		this.serviceExecutor = serviceExecutor;
	}

	public void setBeanName(String arg0) {
		beanName = arg0;
	}

	public void afterPropertiesSet() throws Exception {
		if (context == null)
			throw new IllegalArgumentException("ApplicationContext is required");
		if (serviceExecutor == null && next == null)
			throw new IllegalArgumentException("Error initializing Bean, ServiceExecutor or Next is required");
		else
			return;
	}

	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		context = arg0;
	}

}
