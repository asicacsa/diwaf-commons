// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   OrquestControllerImpl.java

package es.di.framework.controllers;

import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;



// Referenced classes of package es.di.framework.controllers:
//			OrquestController

public class OrquestControllerImpl extends OrquestController {

	public OrquestControllerImpl() {
	}

	public ModelAndView handleRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		System.out.println("Estamos en OrquestControllerImpl antes de la llamada a ServiceExecutor...");
		showRequestAttribute(arg0);
		ModelAndView resul = null;
		resul = callToServiceExecutor(arg0, arg1);
		System.out.println("Estamos en OrquestControllerImpl despues de la llamada a ServiceExecutor...");
		showRequestAttribute(arg0);
		System.out.println("Estamos en OrquestControllerImpl antes de la llamada a Next...");
		resul = callToNext(arg0, arg1);
		System.out.println("Estamos en OrquestControllerImpl despues de la llamada a Next...");
		showRequestAttribute(arg0);
		return resul;
	}

	private void showRequestAttribute(HttpServletRequest arg0) throws Exception {
		Enumeration enu = arg0.getAttributeNames();
		Object objeto = null;
		if (enu != null)
			for (; enu.hasMoreElements(); System.out.println((new StringBuilder()).append("Atributo de REQUEST : ").append(objeto.toString()).append(" con valor : ").append(arg0.getAttribute(objeto.toString()).toString()).toString()))
				objeto = enu.nextElement();

		enu = arg0.getParameterNames();
		if (enu != null)
			do {
				if (!enu.hasMoreElements())
					break;
				objeto = enu.nextElement();
				if (objeto != null)
					System.out.println((new StringBuilder()).append("Parametro de REQUEST : ").append(objeto.toString()).append(" con valor : ").append(arg0.getParameter(objeto.toString()).toString()).toString());
			} while (true);
		else
			System.out.println("No hay parametros...");
	}
}
