// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   SimpleXSLTransformerControllerImpl.java

package es.di.framework.controllers;

import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.springframework.web.servlet.ModelAndView;

// Referenced classes of package es.di.framework.controllers:
//			SimpleXSLTransformerController

public class SimpleXSLTransformerControllerImpl extends SimpleXSLTransformerController {

	private String input = null;
	private String xsl = null;
	private String xsl_repo = null;

	public SimpleXSLTransformerControllerImpl() {
		input = null;
		xsl = null;
		xsl_repo = null;
	}

	public ModelAndView handleRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		String entrada = arg0.getParameter(getInput());
		String xsl_var = arg0.getParameter(getXsl());
		javax.xml.transform.Source xml_entrada = new StreamSource(new StringReader(entrada));
		javax.xml.transform.Source xsl_entrada = new StreamSource(new FileReader((new StringBuilder()).append(getXsl_repo()).append(File.separator).append(xsl_var).toString()));
		javax.xml.transform.Result resultado = new StreamResult(arg1.getWriter());
		transform(xml_entrada, xsl_entrada, resultado);
		arg1.getWriter().flush();
		arg1.getWriter().close();
		return null;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getXsl() {
		return xsl;
	}

	public void setXsl(String xsl) {
		this.xsl = xsl;
	}

	public String getXsl_repo() {
		return xsl_repo;
	}

	public void setXsl_repo(String xsl_repo) {
		this.xsl_repo = xsl_repo;
	}

	public void afterPropertiesSet() throws Exception {
		if (xsl == null)
			throw new IllegalArgumentException("XSL file is required");
		if (xsl_repo == null)
			throw new IllegalArgumentException("XSL repository is required");
		else
			return;
	}
}
