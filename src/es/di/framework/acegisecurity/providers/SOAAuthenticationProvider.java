// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   SOAAuthenticationProvider.java

package es.di.framework.acegisecurity.providers;

import com.thoughtworks.xstream.XStream;
import es.di.framework.errores.FrameWorkServerInvokerException;
import es.di.framework.exceptions.BusinessRuleException;
import es.di.framework.service.controller.ISOAServiceController;
import java.util.Iterator;
import java.util.List;
import net.sf.acegisecurity.*;
import net.sf.acegisecurity.context.ContextHolder;
import net.sf.acegisecurity.context.security.SecureContext;
import net.sf.acegisecurity.providers.AuthenticationProvider;
import net.sf.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import net.sf.acegisecurity.providers.dao.*;
import net.sf.acegisecurity.providers.dao.cache.NullUserCache;
import net.sf.acegisecurity.providers.dao.event.*;
import net.sf.acegisecurity.providers.encoding.PasswordEncoder;
import net.sf.acegisecurity.providers.encoding.PlaintextPasswordEncoder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.dao.DataAccessException;
import org.springframework.util.Assert;

public class SOAAuthenticationProvider
	implements AuthenticationProvider, InitializingBean, ApplicationContextAware {

	private static final Log log = LogFactory.getLog(es.di.framework.acegisecurity.providers.SOAAuthenticationProvider.class);
	private ApplicationContext context = null;
	private PasswordEncoder passwordEncoder = null;
	private SaltSource saltSource = null;
	private UserCache userCache = null;
	private boolean forcePrincipalAsString = false;
	private boolean hideUserNotFoundExceptions = false;
	private String serviceName = null;
	private String parameter = null;
	private ISOAServiceController serviceSOA = null;
	private XStream xstream = null;

	public SOAAuthenticationProvider() {
		passwordEncoder = new PlaintextPasswordEncoder();
		userCache = new NullUserCache();
		forcePrincipalAsString = false;
		hideUserNotFoundExceptions = true;
		xstream = new XStream();
	}

	public void afterPropertiesSet() throws Exception {
		Assert.notNull(userCache, "A user cache must be set");
		Assert.notNull(serviceSOA, "A ISOAServiceController must be set");
		Assert.notNull(serviceName, "A serviceName must be set");
		Assert.notNull(parameter, "A parameter must be set");
	}

	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		context = arg0;
	}

	public boolean isForcePrincipalAsString() {
		return forcePrincipalAsString;
	}

	public void setForcePrincipalAsString(boolean forcePrincipalAsString) {
		this.forcePrincipalAsString = forcePrincipalAsString;
	}

	public boolean isHideUserNotFoundExceptions() {
		return hideUserNotFoundExceptions;
	}

	public void setHideUserNotFoundExceptions(boolean hideUserNotFoundExceptions) {
		this.hideUserNotFoundExceptions = hideUserNotFoundExceptions;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public SaltSource getSaltSource() {
		return saltSource;
	}

	public void setSaltSource(SaltSource saltSource) {
		this.saltSource = saltSource;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public ISOAServiceController getServiceSOA() {
		return serviceSOA;
	}

	public void setServiceSOA(ISOAServiceController serviceSOA) {
		this.serviceSOA = serviceSOA;
	}

	public UserCache getUserCache() {
		return userCache;
	}

	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = "NONE_PROVIDED";
		String password = "";
		if (authentication.getPrincipal() != null) {
			username = authentication.getPrincipal().toString();
			password = authentication.getCredentials().toString();
		}
		if (authentication.getPrincipal() instanceof UserDetails) {
			username = ((UserDetails)authentication.getPrincipal()).getUsername();
			password = ((UserDetails)authentication.getPrincipal()).getPassword();
		}
		boolean cacheWasUsed = true;
		UserDetails user = userCache.getUserFromCache(username);
		if (user == null) {
			cacheWasUsed = false;
			try {
				((SecureContext)ContextHolder.getContext()).setAuthentication(authentication);
				user = getUserFromBackend(username, password);
			}
			catch (BadCredentialsException ex) {
				if (context != null)
					context.publishEvent(new AuthenticationFailureUsernameNotFoundEvent(authentication, new User("".equals(username) ? "EMPTY_STRING_PROVIDED" : username, "*****", false, false, false, false, new GrantedAuthority[0])));
				throw ex;
			}
		}
		if (!user.isEnabled()) {
			if (context != null)
				context.publishEvent(new AuthenticationFailureDisabledEvent(authentication, user));
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("The user : ").append(username).append(" is disabled.").toString());
			throw new DisabledException("User is disabled");
		}
		if (!user.isAccountNonExpired()) {
			if (context != null)
				context.publishEvent(new AuthenticationFailureAccountExpiredEvent(authentication, user));
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("The user : ").append(username).append(" account has expired.").toString());
			throw new AccountExpiredException("User account has expired");
		}
		if (!user.isAccountNonLocked()) {
			if (context != null)
				context.publishEvent(new AuthenticationFailureAccountLockedEvent(authentication, user));
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("The user : ").append(username).append(" account is locked.").toString());
			throw new LockedException("User account is locked");
		}
		if (!user.isCredentialsNonExpired()) {
			if (context != null)
				context.publishEvent(new AuthenticationFailureCredentialsExpiredEvent(authentication, user));
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("The user : ").append(username).append(" credentials have expired.").toString());
			throw new CredentialsExpiredException("User credentials have expired");
		}
		if (!cacheWasUsed) {
			userCache.putUserInCache(user);
			if (context != null)
				context.publishEvent(new AuthenticationSuccessEvent(authentication, user));
		}
		Object principalToReturn = user;
		if (forcePrincipalAsString)
			principalToReturn = user.getUsername();
		return createSuccessAuthentication(principalToReturn, authentication, user);
	}

	public boolean supports(Class arg0) {
		return net.sf.acegisecurity.providers.UsernamePasswordAuthenticationToken.class.isAssignableFrom(arg0);
	}

	protected boolean isPasswordCorrect(Authentication authentication, UserDetails user) {
		Object salt = null;
		if (saltSource != null)
			salt = saltSource.getSalt(user);
		if (log.isDebugEnabled()) {
			log.debug("Estamos en la validacion del password : ");
			if (salt != null)
				log.debug((new StringBuilder()).append("El objeto salt que hemos usado : ").append(salt.toString()).toString());
			else
				log.debug("No tenemos objeto Salt");
			log.debug((new StringBuilder()).append("El password encoder : ").append(passwordEncoder.toString()).toString());
			log.debug((new StringBuilder()).append("El password a validar : ").append(user.getPassword()).toString());
			log.debug((new StringBuilder()).append("Las credentials : ").append(authentication.getCredentials()).toString());
		}
		return passwordEncoder.isPasswordValid(user.getPassword(), authentication.getCredentials().toString(), salt);
	}

	protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(principal, authentication.getCredentials(), user.getAuthorities());
		result.setDetails(authentication.getDetails() == null ? null : authentication.getDetails());
		if (log.isDebugEnabled())
			log.debug((new StringBuilder()).append("Username to create success authentication : ").append(result.toString()).toString());
		return result;
	}

	private UserDetails getUserFromBackend(String username, String password) {
		String result;
		try {
			result = serviceSOA.process(serviceName, parameter);
		}
		catch (UsernameNotFoundException notFound) {
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("Username not found in the service : ").append(serviceName).toString(), notFound);
			if (hideUserNotFoundExceptions)
				throw new BadCredentialsException("Bad credentials presented");
			else
				throw notFound;
		}
		catch (DataAccessException repositoryProblem) {
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("DataAccessException in the service : ").append(serviceName).toString(), repositoryProblem);
			throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
		}
		catch (BusinessRuleException bre) {
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("BussinesRuleException in the service : ").append(serviceName).toString(), bre);
			throw new AuthenticationServiceException(bre.getMessage(), bre);
		}
		catch (FrameWorkServerInvokerException fwsie) {
			if (log.isDebugEnabled())
				log.debug((new StringBuilder()).append("FrameWorkServerException in the service :").append(serviceName).toString(), fwsie);
			if (fwsie.get_statusCode() != null && fwsie.get_statusCode().intValue() == 401)
				throw new BadCredentialsException("Bad credentials presented", fwsie);
			else
				throw new AuthenticationServiceException(fwsie.getMessage(), fwsie);
		}
		catch (Exception ex) {
			if (log.isDebugEnabled()) {
				log.debug((new StringBuilder()).append("Exception in the service : ").append(serviceName).toString());
				ex.printStackTrace();
			}
			throw new AuthenticationServiceException(ex.getMessage(), ex);
		}
		if (result == null)
			throw new AuthenticationServiceException((new StringBuilder()).append("ServiceSOA ").append(serviceName).append(" returned null, which is an interface contract violation").toString());
		if (log.isDebugEnabled())
			log.debug((new StringBuilder()).append("Esto es lo que recibimos en la llamada : ").append(result).toString());
		List lista = (List)xstream.fromXML(result);
		GrantedAuthority authorities[] = new GrantedAuthority[lista.size()];
		int i = 0;
		for (Iterator iter = lista.iterator(); iter.hasNext();) {
			String element = (String)iter.next();
			GrantedAuthority aux_granted = new GrantedAuthorityImpl(element);
			authorities[i] = aux_granted;
			i++;
		}

		UserDetails loadedUser = new User(username, password, true, true, true, true, authorities);
		return loadedUser;
	}

}
