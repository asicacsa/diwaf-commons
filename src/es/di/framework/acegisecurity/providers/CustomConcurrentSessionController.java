// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   CustomConcurrentSessionController.java

package es.di.framework.acegisecurity.providers;

import java.util.*;
import javax.servlet.http.HttpSession;
import net.sf.acegisecurity.*;
import net.sf.acegisecurity.providers.*;
import net.sf.acegisecurity.ui.WebAuthenticationDetails;
import net.sf.acegisecurity.ui.session.HttpSessionDestroyedEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.*;

public class CustomConcurrentSessionController
	implements ConcurrentSessionController, ApplicationListener, ApplicationContextAware {

	private static final Log log = LogFactory.getLog(es.di.framework.acegisecurity.providers.CustomConcurrentSessionController.class);
	protected Map principalsToSessions = null;
	protected Map sessionsToPrincipals = null;
	protected Set sessionSet = null;
	private ApplicationContext applicationContext = null;
	private AuthenticationTrustResolver trustResolver = null;
	private int maxSessions = 0;

	public CustomConcurrentSessionController() {
		principalsToSessions = new HashMap();
		sessionsToPrincipals = new HashMap();
		sessionSet = new HashSet();
		trustResolver = new AuthenticationTrustResolverImpl();
		maxSessions = 1;
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setMaxSessions(int maxSessions) {
		this.maxSessions = maxSessions;
	}

	public int getMaxSessions() {
		return maxSessions;
	}

	public void setTrustResolver(AuthenticationTrustResolver trustResolver) {
		this.trustResolver = trustResolver;
	}

	public AuthenticationTrustResolver getTrustResolver() {
		return trustResolver;
	}

	public void afterAuthentication(Authentication request, Authentication response) throws ConcurrentLoginException {
		enforceConcurrentLogins(response);
		if (request.getDetails() instanceof WebAuthenticationDetails) {
			String sessionId = ((WebAuthenticationDetails)request.getDetails()).getSessionId();
			addSession(determineSessionPrincipal(response), sessionId);
		}
	}

	public void beforeAuthentication(Authentication request) throws ConcurrentLoginException {
		enforceConcurrentLogins(request);
	}

	public void onApplicationEvent(ApplicationEvent event) {
		if (event instanceof HttpSessionDestroyedEvent) {
			String sessionId = ((HttpSession)event.getSource()).getId();
			removeSession(sessionId);
		}
	}

	protected boolean isActiveSession(Object principal, String sessionId) {
		Set sessions = (Set)principalsToSessions.get(principal);
		if (sessions == null)
			return false;
		else
			return sessions.contains(sessionId);
	}

	protected void addSession(Object principal, String sessionId) {
		Set sessions = (Set)principalsToSessions.get(principal);
		if (sessions == null) {
			sessions = new HashSet();
			principalsToSessions.put(principal, sessions);
		}
		sessions.add(sessionId);
		Object aux_principal = getPrincipalOfSession(sessionId);
		if (log.isDebugEnabled())
			log.debug((new StringBuilder()).append("Este es el principal pasado como parametro : ").append(principal.toString()).toString());
		if (aux_principal != null && !principal.equals(aux_principal)) {
			Set aux_sessions = (Set)principalsToSessions.get(aux_principal);
			if (aux_sessions != null) {
				aux_sessions.remove(sessionId);
				if (log.isInfoEnabled())
					log.info((new StringBuilder()).append("Eliminado la session ").append(sessionId).append(" del principal ").append(aux_principal.toString()).toString());
				if (aux_sessions.isEmpty()) {
					principalsToSessions.remove(aux_principal);
					if (log.isDebugEnabled())
						log.debug("Principal se queda sin session's, de modo que lo eliminamos...");
				}
			}
		}
		sessionsToPrincipals.put(sessionId, principal);
	}

	private Object getPrincipalOfSession(String sessionId) {
		Object principal = sessionsToPrincipals.get(sessionId);
		return principal;
	}

	protected int countSessions(Object principal) {
		Set set = (Set)principalsToSessions.get(principal);
		if (set == null)
			return 0;
		else
			return set.size();
	}

	protected Object determineSessionPrincipal(Authentication auth) {
		if (auth.getPrincipal() instanceof UserDetails)
			return ((UserDetails)auth.getPrincipal()).getUsername();
		else
			return auth.getPrincipal().toString();
	}

	protected void enforceConcurrentLogins(Authentication request) throws ConcurrentLoginException {
		if (maxSessions < 1)
			return;
		if (trustResolver.isAnonymous(request))
			return;
		if (request.getDetails() instanceof WebAuthenticationDetails) {
			String sessionId = ((WebAuthenticationDetails)request.getDetails()).getSessionId();
			Object principal = determineSessionPrincipal(request);
			if (!isActiveSession(principal, sessionId) && maxSessions == countSessions(principal)) {
				publishViolationEvent(request);
				throw new ConcurrentLoginException((new StringBuilder()).append(principal).append(" has reached the maximum concurrent logins").toString());
			}
		}
	}

	protected void publishViolationEvent(Authentication auth) {
		getApplicationContext().publishEvent(new ConcurrentSessionViolationEvent(auth));
	}

	protected void removeSession(String sessionId) {
		Object associatedPrincipal = sessionsToPrincipals.get(sessionId);
		if (associatedPrincipal != null) {
			Set sessions = (Set)principalsToSessions.get(associatedPrincipal);
			sessions.remove(sessionId);
			if (sessions.isEmpty())
				principalsToSessions.remove(associatedPrincipal);
			sessionsToPrincipals.remove(sessionId);
		}
	}

}
