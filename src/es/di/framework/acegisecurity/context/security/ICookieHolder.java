// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   ICookieHolder.java

package es.di.framework.acegisecurity.context.security;

import org.apache.commons.httpclient.Cookie;

public interface ICookieHolder {

	public abstract Cookie getCookie();

	public abstract void setCookie(Cookie cookie);
}
