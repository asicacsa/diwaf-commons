// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   CustomHttpInvokerProxyFactoryBean.java

package es.di.framework.acegisecurity.context.httpinvoker;

import java.net.MalformedURLException;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

// Referenced classes of package es.di.framework.acegisecurity.context.httpinvoker:
//			CustomHttpInvokerClientInterceptor

public class CustomHttpInvokerProxyFactoryBean extends CustomHttpInvokerClientInterceptor
	implements FactoryBean, InitializingBean {

	private Object serviceProxy = null;

	public CustomHttpInvokerProxyFactoryBean() {
	}

	@Override
	public void afterPropertiesSet() /*throws MalformedURLException*/ {
		if (getServiceInterface() == null) {
			throw new IllegalArgumentException("serviceInterface is required");
		} else {
			serviceProxy = ProxyFactory.getProxy(getServiceInterface(), this);
			return;
		}
	}

	public Object getObject() {
		return serviceProxy;
	}

	public Class getObjectType() {
		return getServiceInterface();
	}

	public boolean isSingleton() {
		return true;
	}
}
