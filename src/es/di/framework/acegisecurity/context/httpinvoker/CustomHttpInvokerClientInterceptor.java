// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   CustomHttpInvokerClientInterceptor.java

package es.di.framework.acegisecurity.context.httpinvoker;

import es.di.framework.errores.FrameWorkServerInvokerException;
import java.io.IOException;
import org.springframework.remoting.httpinvoker.HttpInvokerClientInterceptor;
import org.springframework.remoting.httpinvoker.HttpInvokerRequestExecutor;
import org.springframework.remoting.support.RemoteInvocation;
import org.springframework.remoting.support.RemoteInvocationResult;

// Referenced classes of package es.di.framework.acegisecurity.context.httpinvoker:
//			CustomHttpInvokerClientConfiguration

public class CustomHttpInvokerClientInterceptor extends HttpInvokerClientInterceptor
	implements CustomHttpInvokerClientConfiguration {

	public CustomHttpInvokerClientInterceptor() {
	}

	protected RemoteInvocationResult executeRequest(RemoteInvocation invocation) throws IOException, ClassNotFoundException, Exception {
		RemoteInvocationResult remote = null;
		try {
			remote = getHttpInvokerRequestExecutor().executeRequest(this, invocation);
		}
		catch (FrameWorkServerInvokerException e) {
			if (e.get_statusCode() != null && e.get_statusCode().intValue() == 401)
				remote = getHttpInvokerRequestExecutor().executeRequest(this, invocation);
			else
				throw e;
		}
		catch (Exception e) {
			throw e;
		}
		return remote;
	}
}
