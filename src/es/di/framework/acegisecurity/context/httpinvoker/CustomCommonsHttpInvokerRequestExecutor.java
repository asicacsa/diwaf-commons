// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   CustomCommonsHttpInvokerRequestExecutor.java

package es.di.framework.acegisecurity.context.httpinvoker;

import es.di.framework.acegisecurity.context.security.ICookieHolder;
import es.di.framework.acegisecurity.utils.Envolver;
import es.di.framework.acegisecurity.utils.UserSessionCache;
import es.di.framework.errores.FrameWorkServerInvokerException;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import net.sf.acegisecurity.Authentication;
import net.sf.acegisecurity.UserDetails;
import net.sf.acegisecurity.context.ContextHolder;
import net.sf.acegisecurity.context.security.SecureContext;
import net.sf.acegisecurity.providers.dao.User;
import net.sf.acegisecurity.ui.WebAuthenticationDetails;
import net.sf.ehcache.Element;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.cookie.CookieSpecBase;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.SSLProtocolSocketFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.remoting.httpinvoker.CommonsHttpInvokerRequestExecutor;
import org.springframework.remoting.httpinvoker.HttpInvokerClientConfiguration;
import org.springframework.remoting.support.RemoteInvocationResult;

// Referenced classes of package es.di.framework.acegisecurity.context.httpinvoker:
//			CustomHttpInvokerClientConfiguration

public class CustomCommonsHttpInvokerRequestExecutor extends CommonsHttpInvokerRequestExecutor {

	private static final Log log = LogFactory.getLog(es.di.framework.acegisecurity.context.httpinvoker.CustomCommonsHttpInvokerRequestExecutor.class);
	private HttpClient httpClient = null;
	private Object userCache = null;
	private boolean isSecure = false;
	private static String HEADER_SET_COOKIE = "Set-Cookie";

	public boolean getIsSecure() {
		return isSecure;
	}

	public void setIsSecure(boolean isSecure) {
		this.isSecure = isSecure;
	}

	public Object getUserCache() {
		return userCache;
	}

	public void setUserCache(Object userCache) {
		this.userCache = userCache;
	}

	private void addUsername(String sessionID, String username) {
		try {
			((UserSessionCache)userCache).setElementUsername(sessionID, username);
		}
		catch (Exception e) {
			if (logger.isDebugEnabled())
				logger.debug((new StringBuilder()).append("Exception when is adding a Username into UserCache : ").append(e.getMessage()).toString());
		}
	}

	private void addCookie(String sessionID, Cookie cookie, String userName) {
		Object c = ContextHolder.getContext();
		if (c != null && (c instanceof SecureContext)) {
			SecureContext sc = (SecureContext)c;
			if (sc instanceof ICookieHolder) {
				ICookieHolder iCookieHolder = (ICookieHolder)sc;
				iCookieHolder.setCookie(cookie);
			} else {
				logger.fatal("SecurityContext no implementa el tipo ICookieHolder.");
			}
		}
	}

	private Cookie treatPostMethod(PostMethod postMethod) {
		Header header = null;
		CookieSpecBase spec = new CookieSpecBase();
		header = postMethod.getResponseHeader(HEADER_SET_COOKIE);
		Cookie cookie[] = null;
		try {
			if (header != null) {
				if (logger.isInfoEnabled())
					logger.info("Hay cookie...");
				cookie = spec.parse(postMethod.getHostConfiguration().getHost(), postMethod.getHostConfiguration().getPort(), postMethod.getPath(), isSecure, header);
			} else {
				if (logger.isInfoEnabled())
					logger.info("No hay cookie...");
				cookie = null;
			}
		}
		catch (Exception ex) {
			if (logger.isDebugEnabled())
				logger.error((new StringBuilder()).append("Exception when is parsing Cookie : ").append(ex.getMessage()).toString());
			else
				ex.printStackTrace();
		}
		if (cookie == null || cookie.length <= 0)
			return null;
		else
			return cookie[0];
	}

	public CustomCommonsHttpInvokerRequestExecutor() {
		httpClient = new HttpClient(new MultiThreadedHttpConnectionManager());
	}

	public CustomCommonsHttpInvokerRequestExecutor(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	protected RemoteInvocationResult doExecuteRequest(HttpInvokerClientConfiguration config, ByteArrayOutputStream baos) throws IOException, ClassNotFoundException {
		return doExecuteRequest((CustomHttpInvokerClientConfiguration)config, baos);
	}

	private RemoteInvocationResult doExecuteRequest(CustomHttpInvokerClientConfiguration config, ByteArrayOutputStream baos) throws IOException, ClassNotFoundException {
		RemoteInvocationResult remoteInvocationResult;
		String sessionID;
		PostMethod postMethod;
		HttpState httpState;
		Header header;
		remoteInvocationResult = null;
		sessionID = getSessionID();
		String cacheUserName = null;
		try {
			cacheUserName = getCacheUserName(sessionID);
			if (logger.isInfoEnabled())
				logger.info((new StringBuilder()).append("The cache UserName : ").append(cacheUserName).toString());
		}
		catch (NullPointerException npe) {
			cacheUserName = null;
		}
		catch (Exception ex) {
			if (logger.isDebugEnabled())
				logger.debug((new StringBuilder()).append("Exception when is finding UserName into UserCache : ").append(ex.getMessage()).toString(), ex);
		}
		Cookie serverCookie = getAuthorizationCookieSession();
		postMethod = createPostMethod(config);
		httpState = createHttpState(config);
		header = null;
		if (serverCookie != null) {
			setCookieIntoHttpState(httpState, serverCookie);
			if (cacheUserName != null) {
				if (!cacheUserName.equals(getActuallyUserName())) {
					addCookie(sessionID, serverCookie, getActuallyUserName());
					header = getAuthorizationHeader();
				}
			} else {
				header = getAuthorizationHeader();
			}
		} else {
			header = getAuthorizationHeader();
		}
		Cookie cookie_response = null;
		setRequestBody(config, postMethod, baos);
		if (logger.isInfoEnabled()) {
			logger.info((new StringBuilder()).append("Header de invocacion : ").append(header).toString());
			logger.info((new StringBuilder()).append("HttpState de invocacion : ").append(httpState.toString()).toString());
		}
		executePostMethod(config, getHttpClient(), postMethod, httpState, header);
		InputStream responseBody = getResponseBody(config, postMethod);
		cookie_response = treatPostMethod(postMethod);
		StatusLine statusLine = postMethod.getStatusLine();
		try {
			remoteInvocationResult = readRemoteInvocationResult(responseBody, config.getCodebaseUrl());
			if (logger.isInfoEnabled()) {
				logger.info((new StringBuilder()).append("Status Code recibido : ").append(statusLine.getStatusCode()).toString());
				logger.info((new StringBuilder()).append("Peticion correcta, STATUS CODE : ").append(statusLine.getStatusCode()).toString());
			}
			if (cookie_response != null)
				addCookie(sessionID, cookie_response, getActuallyUserName());
			else
				addUsername(sessionID, getActuallyUserName());
		}
		catch (IOException ex) {
			if (cookie_response != null) {
				if (logger.isInfoEnabled())
					logger.info((new StringBuilder()).append("La session ").append(sessionID).append(" No estamos validados, , STATUS CODE : ").append(statusLine.getStatusCode()).append(" cookie ").append(cookie_response.toString()).toString());
				addCookie(sessionID, cookie_response, null);
			} else {
				if (logger.isInfoEnabled())
					logger.info((new StringBuilder()).append("No estamos validados, , STATUS CODE : ").append(statusLine.getStatusCode()).append(" cookie nula...").toString());
				addUsername(sessionID, null);
			}
			if (statusLine != null)
				throw new FrameWorkServerInvokerException(ex, 0, new Integer(statusLine.getStatusCode()));
			else
				throw ex;
		}
		catch (Exception ex) {
			if (log.isDebugEnabled())
				log.debug("Excepcion en la peticion : ", ex);
			throw new FrameWorkServerInvokerException(ex, 0, new Integer(0));
		}
		postMethod.releaseConnection();
		//break MISSING_BLOCK_LABEL_688;
		Exception exception;
		//exception;
		postMethod.releaseConnection();
		//throw exception;
		return remoteInvocationResult;
	}

	protected PostMethod createPostMethod(HttpInvokerClientConfiguration config) throws IOException {
		return createPostMethod((CustomHttpInvokerClientConfiguration)config);
	}

	private PostMethod createPostMethod(CustomHttpInvokerClientConfiguration config) throws IOException {
		PostMethod postMethod = new PostMethod(config.getServiceUrl());
		postMethod.setRequestHeader("Content-Type", "application.x-java-serialized-object");
		return postMethod;
	}

	private HttpState createHttpState(CustomHttpInvokerClientConfiguration config) {
		HttpState httpState = new HttpState();
		return httpState;
	}

	private void setCookieIntoHttpState(HttpState httpState, Cookie cookie) {
		httpState.addCookie(cookie);
	}

	private HostConfiguration createHostConfiguration(CustomHttpInvokerClientConfiguration config) {
		HostConfiguration hostConfiguration = new HostConfiguration();
		try {
			URL url = new URL(config.getServiceUrl());
			Protocol protocol = new Protocol(url.getProtocol(), new SSLProtocolSocketFactory(), url.getPort());
			hostConfiguration.setHost(url.getHost(), url.getPort(), protocol);
		}
		catch (MalformedURLException ex) {
			if (logger.isDebugEnabled())
				logger.debug((new StringBuilder()).append("Exception when is recovering Cookie session ID of server : ").append(ex.getMessage()).toString());
		}
		return hostConfiguration;
	}

	protected void setRequestBody(HttpInvokerClientConfiguration config, PostMethod postMethod, ByteArrayOutputStream baos) throws IOException {
		setRequestBody((CustomHttpInvokerClientConfiguration)config, postMethod, baos);
	}

	protected void setRequestBody(CustomHttpInvokerClientConfiguration config, PostMethod postMethod, ByteArrayOutputStream baos) throws IOException {
		postMethod.setRequestBody(new ByteArrayInputStream(baos.toByteArray()));
	}

	/**
	 * @deprecated Method executePostMethod is deprecated
	 */

	protected void executePostMethod(HttpInvokerClientConfiguration config, HttpClient httpClient, PostMethod postMethod) throws IOException {
		executePostMethod((CustomHttpInvokerClientConfiguration)config, httpClient, postMethod);
	}

	/**
	 * @deprecated Method executePostMethod is deprecated
	 */

	protected void executePostMethod(CustomHttpInvokerClientConfiguration config, HttpClient httpClient, PostMethod postMethod) throws IOException {
		httpClient.executeMethod(postMethod);
	}

	private void executePostMethod(CustomHttpInvokerClientConfiguration config, HttpClient httpClient, PostMethod postMethod, HttpState httpState, Header authorization) throws IOException {
		httpClient.setState(httpState);
		if (authorization != null)
			postMethod.addRequestHeader(authorization);
		HostConfiguration host = createHostConfiguration(config);
		if (host != null)
			httpClient.setHostConfiguration(host);
		httpClient.executeMethod(postMethod);
	}

	private Cookie getAuthorizationCookieSession() {
		Cookie authorization = null;
		if (logger.isInfoEnabled())
			logger.info("Call to getAuthorizationCookieSession : ");
		if (ContextHolder.getContext() != null && (ContextHolder.getContext() instanceof SecureContext)) {
			SecureContext sc = (SecureContext)ContextHolder.getContext();
			if (sc instanceof ICookieHolder) {
				ICookieHolder iCookieHolder = (ICookieHolder)sc;
				authorization = iCookieHolder.getCookie();
			}
		}
		if (logger.isInfoEnabled())
			logger.info((new StringBuilder()).append("Call to getAuthorizationCookieSession response : ").append(authorization).toString());
		return authorization;
	}

	private String getSessionID() {
		String sessionID = null;
		if (logger.isInfoEnabled())
			logger.info("Call to getSessionID : ");
		if (ContextHolder.getContext() != null && (ContextHolder.getContext() instanceof SecureContext)) {
			Authentication auth = ((SecureContext)ContextHolder.getContext()).getAuthentication();
			if (auth != null && auth.getDetails() != null && (auth.getDetails() instanceof WebAuthenticationDetails)) {
				WebAuthenticationDetails authentication = (WebAuthenticationDetails)auth.getDetails();
				sessionID = authentication.getSessionId();
			} else
			if (logger.isDebugEnabled())
				logger.debug((new StringBuilder()).append("Unable to recover Authentication : ").append(ContextHolder.getContext()).append("; did not provide valid Authentication: ").append(auth).toString());
		}
		if (logger.isInfoEnabled())
			logger.info((new StringBuilder()).append("Call to getSessionID reponse : ").append(sessionID).toString());
		return sessionID;
	}

	private Header getAuthorizationHeader() {
		String name = null;
		String pass = null;
		String base64 = null;
		Header authorization = null;
		if (logger.isInfoEnabled())
			logger.info("Call to getAuthorizationHeader. ");
		try {
			if (ContextHolder.getContext() != null && (ContextHolder.getContext() instanceof SecureContext)) {
				Authentication auth = ((SecureContext)ContextHolder.getContext()).getAuthentication();
				if (auth != null && auth.getPrincipal() != null && auth.getCredentials() != null) {
					if (auth.getPrincipal() instanceof UserDetails)
						name = ((UserDetails)auth.getPrincipal()).getUsername().toString();
					else
					if (auth.getPrincipal() instanceof User)
						name = ((User)auth.getPrincipal()).toString();
					else
						name = auth.getPrincipal().toString();
					if (name != null) {
						pass = auth.getCredentials().toString();
						base64 = (new StringBuilder()).append(name).append(":").append(pass).toString();
						authorization = new Header("Authorization", (new StringBuilder()).append("Basic ").append(new String(Base64.encodeBase64(base64.getBytes()))).toString());
					}
					if (logger.isDebugEnabled())
						logger.debug((new StringBuilder()).append("HttpInvocation now presenting via BASIC authentication ContextHolder-derived: ").append(auth.toString()).toString());
				} else
				if (logger.isDebugEnabled())
					logger.debug((new StringBuilder()).append("Unable to set BASIC authentication header as ContextHolder: ").append(ContextHolder.getContext()).append("; did not provide valid Authentication: ").append(auth).toString());
			} else
			if (logger.isDebugEnabled())
				logger.debug("Unable to set BASIC authentication header as ContextHolder, ContextHolder does not exist. ");
		}
		catch (Exception ex) {
			if (logger.isDebugEnabled())
				logger.info((new StringBuilder()).append("Exception in getAuthorizationHeader ").append(ex.getMessage()).toString(), ex);
		}
		if (logger.isInfoEnabled())
			if (authorization != null)
				logger.info((new StringBuilder()).append("Calling getAuthorizationHeader :").append(authorization.toString()).toString());
			else
				logger.info("El header es nulo.");
		if (logger.isInfoEnabled())
			logger.info((new StringBuilder()).append("Call to getAuthorizationHeader response : ").append(authorization).toString());
		return authorization;
	}

	private String getActuallyUserName() {
		String name = null;
		if (ContextHolder.getContext() != null && (ContextHolder.getContext() instanceof SecureContext)) {
			Authentication auth = ((SecureContext)ContextHolder.getContext()).getAuthentication();
			if (auth != null && auth.getPrincipal() != null && auth.getCredentials() != null) {
				if (auth.getPrincipal() instanceof UserDetails)
					name = ((UserDetails)auth.getPrincipal()).getUsername().toString();
				else
				if (auth.getPrincipal() instanceof User)
					name = ((User)auth.getPrincipal()).toString();
				else
					name = auth.getPrincipal().toString();
				if (logger.isDebugEnabled())
					logger.debug((new StringBuilder()).append("HttpInvocation now presenting via BASIC authentication ContextHolder-derived: ").append(auth.toString()).toString());
				if (logger.isDebugEnabled())
					logger.debug((new StringBuilder()).append("The name of log user : ").append(name).toString());
			} else
			if (logger.isDebugEnabled())
				logger.debug((new StringBuilder()).append("Unable to set BASIC authentication header as ContextHolder: ").append(ContextHolder.getContext()).append("; did not provide valid Authentication: ").append(auth).toString());
		}
		return name;
	}

	private String getCacheUserName(String sessionID) throws Exception {
		String username = null;
		username = (String)((Envolver)((UserSessionCache)userCache).getElement(sessionID).getValue()).getUsername();
		return username;
	}

	protected InputStream getResponseBody(CustomHttpInvokerClientConfiguration config, PostMethod postMethod) throws IOException {
		return postMethod.getResponseBodyAsStream();
	}

}
