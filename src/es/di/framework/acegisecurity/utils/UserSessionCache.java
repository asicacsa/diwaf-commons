// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   UserSessionCache.java

package es.di.framework.acegisecurity.utils;

import javax.servlet.http.HttpSession;
import net.sf.acegisecurity.Authentication;
import net.sf.acegisecurity.providers.ConcurrentSessionViolationEvent;
import net.sf.acegisecurity.ui.session.HttpSessionCreatedEvent;
import net.sf.acegisecurity.ui.session.HttpSessionDestroyedEvent;
import net.sf.ehcache.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.*;

// Referenced classes of package es.di.framework.acegisecurity.utils:
//			Envolver

public class UserSessionCache
	implements ApplicationContextAware, ApplicationListener, InitializingBean {

	protected static final Log logger = LogFactory.getLog(es.di.framework.acegisecurity.utils.UserSessionCache.class);
	private ApplicationContext context = null;
	private String userCacheName = null;
	private CacheManager cacheManager = null;

	public UserSessionCache() {
		userCacheName = null;
		cacheManager = null;
	}

	public void afterPropertiesSet() throws Exception {
		if (getCacheManager() == null)
			throw new IllegalArgumentException("CacheManager is required");
		else
			return;
	}

	public Element getElement(String idSession) throws Exception {
		Element element = null;
		Cache aux = cacheManager.getCache(userCacheName);
		try {
			element = aux.get(idSession);
		}
		catch (IllegalStateException ise) {
			if (logger.isDebugEnabled())
				logger.debug("The cache no is alive");
			throw ise;
		}
		catch (CacheException cex) {
			if (logger.isDebugEnabled()) {
				logger.debug((new StringBuilder()).append("CacheException : ").append(cex.getMessage()).toString());
				throw cex;
			}
		}
		if (element == null && logger.isInfoEnabled())
			logger.info((new StringBuilder()).append("Element not found into the Cache : ").append(idSession).toString());
		return element;
	}

	public void setElementValue(String idSession, Object cookie, Object userName) throws Exception {
		Element element = null;
		Cache aux = cacheManager.getCache(userCacheName);
		try {
			element = aux.get(idSession);
		}
		catch (IllegalStateException ise) {
			if (logger.isDebugEnabled())
				logger.debug("The cache no is alive");
			throw ise;
		}
		catch (CacheException cex) {
			if (logger.isDebugEnabled())
				logger.debug((new StringBuilder()).append("CacheException : ").append(cex.getMessage()).toString());
			throw cex;
		}
		if (element != null) {
			((Envolver)element.getValue()).setCookie(cookie);
			((Envolver)element.getValue()).setUsername(userName);
			aux.put(element);
		}
	}

	public void setElementCookie(String idSession, Object value) throws Exception {
		Element element = null;
		Cache aux = cacheManager.getCache(userCacheName);
		try {
			element = aux.get(idSession);
		}
		catch (IllegalStateException ise) {
			if (logger.isDebugEnabled())
				logger.debug("The cache no is alive");
			throw ise;
		}
		catch (CacheException cex) {
			if (logger.isDebugEnabled())
				logger.debug((new StringBuilder()).append("CacheException : ").append(cex.getMessage()).toString());
			throw cex;
		}
		if (element != null) {
			((Envolver)element.getValue()).setCookie(value);
			aux.put(element);
		}
	}

	public void setElementUsername(String idSession, Object value) throws Exception {
		Element element = null;
		Cache aux = cacheManager.getCache(userCacheName);
		try {
			element = aux.get(idSession);
		}
		catch (IllegalStateException ise) {
			if (logger.isDebugEnabled())
				logger.debug("The cache no is alive");
			throw ise;
		}
		catch (CacheException cex) {
			if (logger.isDebugEnabled())
				logger.debug((new StringBuilder()).append("CacheException : ").append(cex.getMessage()).toString());
			throw cex;
		}
		if (element != null) {
			((Envolver)element.getValue()).setUsername(value);
			aux.put(element);
		}
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public String getUserCacheName() {
		return userCacheName;
	}

	public void setUserCacheName(String userCache) {
		userCacheName = userCache;
	}

	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		context = arg0;
	}

	private void onApplicationEvent(HttpSessionCreatedEvent arg0) {
		if (logger.isInfoEnabled())
			logger.info((new StringBuilder()).append("New element into the Cache ").append(arg0.getSession().getId()).toString());
		Cache aux = cacheManager.getCache(userCacheName);
		aux.put(getNewElement(arg0.getSession()));
	}

	private Element getNewElement(HttpSession session) {
		Element element = new Element(session.getId(), new Envolver());
		return element;
	}

	private void onApplicationEvent(HttpSessionDestroyedEvent arg0) {
		Cache aux = cacheManager.getCache(userCacheName);
		try {
			aux.remove(arg0.getSession().getId());
			if (aux.get(arg0.getSession().getId()) != null) {
				if (logger.isDebugEnabled())
					logger.debug((new StringBuilder()).append("Deleted element into the Cache ").append(arg0.getSession().getId()).toString());
			} else
			if (logger.isInfoEnabled())
				logger.info((new StringBuilder()).append("Deleted element into the Cache ").append(arg0.getSession().getId()).toString());
		}
		catch (Exception ex) {
			if (logger.isErrorEnabled())
				logger.error((new StringBuilder()).append("Error when remove the element ").append(arg0.getSession().getId()).toString(), ex);
		}
	}

	public void onApplicationEvent(ApplicationEvent arg0) {
		if (arg0 instanceof HttpSessionCreatedEvent)
			onApplicationEvent((HttpSessionCreatedEvent)arg0);
		if (arg0 instanceof HttpSessionDestroyedEvent)
			onApplicationEvent((HttpSessionDestroyedEvent)arg0);
		if (arg0 instanceof ConcurrentSessionViolationEvent)
			onApplicationEvent((ConcurrentSessionViolationEvent)arg0);
	}

	private void onApplicationEvent(ConcurrentSessionViolationEvent arg0) {
		if (logger.isInfoEnabled())
			logger.info((new StringBuilder()).append("Try to login in the aplication for ").append(arg0.getAuthentication().getName()).toString());
	}

}
