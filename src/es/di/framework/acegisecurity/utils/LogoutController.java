// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   LogoutController.java

package es.di.framework.acegisecurity.utils;

import java.io.PrintWriter;
import javax.servlet.http.*;
import net.sf.acegisecurity.context.ContextHolder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

// Referenced classes of package es.di.framework.acegisecurity.utils:
//			ILogout

public class LogoutController
	implements Controller, ApplicationContextAware, ILogout {

	private static final Log log = LogFactory.getLog(es.di.framework.acegisecurity.utils.LogoutController.class);
	private ApplicationContext context = null;
	private String invokerBeanLogout = null;

	public LogoutController() {
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse arg1) throws Exception {
		if (invokerBeanLogout != null) {
			Object auxiliar = context.getBean(invokerBeanLogout);
			if (auxiliar != null)
				if (!(auxiliar instanceof ILogout)) {
					if (log.isDebugEnabled())
						log.debug("El bean configurado para el logout no cumple la Interface ILogout.");
				} else {
					((ILogout)auxiliar).logout();
				}
		}
		if (request instanceof HttpServletRequest) {
			HttpSession ses = request.getSession(false);
			if (ses != null)
				ses.invalidate();
		}
		arg1.getWriter().print("<respuesta>OK</respuesta>");
		arg1.getWriter().flush();
		arg1.getWriter().close();
		return null;
	}

	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		context = arg0;
	}

	public void setInvokerBeanLogout(String invokerBeanLogout) {
		this.invokerBeanLogout = invokerBeanLogout;
	}

	public void logout() throws Exception {
		if (log.isDebugEnabled())
			log.debug((new StringBuilder()).append("The ").append(ContextHolder.getContext().toString()).append(" is logging out.").toString());
		ContextHolder.setContext(null);
		if (log.isDebugEnabled())
			if (ContextHolder.getContext() == null)
				log.debug("Logout succes.");
			else
				log.debug("Logout not succes.");
	}

}
