// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   Envolver.java

package es.di.framework.acegisecurity.utils;

import java.io.Serializable;

public class Envolver
	implements Serializable {

	private static final long serialVersionUID = 0x4a0fa2abd795ab03L;
	private Object heart = null;
	private Object username = null;

	public Envolver() {
		heart = null;
		username = null;
	}

	public Object getCookie() {
		return heart;
	}

	public void setCookie(Object heart) {
		this.heart = heart;
	}

	public Object getUsername() {
		return username;
	}

	public void setUsername(Object username) {
		this.username = username;
	}
}
