// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) definits fieldsfirst nonlb space 
// Source File Name:   DiwafUserDetails.java

package es.di.framework.acegisecurity;

import java.util.HashMap;
import java.util.Map;
import net.sf.acegisecurity.GrantedAuthority;
import net.sf.acegisecurity.UserDetails;
import org.springframework.util.Assert;

public class DiwafUserDetails
	implements UserDetails {

	private boolean isAccountNonExpired = false;
	private boolean isAccountNonLocked = false;
	private GrantedAuthority grantedAuthority[] = null;
	private boolean isCredentialsNonExpired = false;
	private boolean isEnabled = false;
	private String username = null;
	private String password = null;
	private Map propiedades = null;

	public DiwafUserDetails() {
		isAccountNonExpired = true;
		isAccountNonLocked = true;
		grantedAuthority = null;
		isCredentialsNonExpired = true;
		isEnabled = true;
		propiedades = new HashMap();
		throw new IllegalArgumentException("Cannot use default constructor");
	}

	public DiwafUserDetails(UserDetails user) {
		isAccountNonExpired = true;
		isAccountNonLocked = true;
		grantedAuthority = null;
		isCredentialsNonExpired = true;
		isEnabled = true;
		propiedades = new HashMap();
		if (user != null)
			setDiwafUserDetails(user.getUsername(), user.getPassword(), user.isEnabled(), user.isAccountNonExpired(), user.isCredentialsNonExpired(), user.isAccountNonLocked(), user.getAuthorities());
		else
			throw new IllegalArgumentException("The User cannot be null.");
	}

	public DiwafUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, GrantedAuthority authorities[]) throws IllegalArgumentException {
		isAccountNonExpired = true;
		isAccountNonLocked = true;
		grantedAuthority = null;
		isCredentialsNonExpired = true;
		isEnabled = true;
		propiedades = new HashMap();
		setDiwafUserDetails(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	private void setDiwafUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, GrantedAuthority authorities[]) {
		if (username == null || "".equals(username) || password == null || authorities == null)
			throw new IllegalArgumentException("Cannot pass null or empty values to constructor");
		setAccountNonExpired(accountNonExpired);
		for (int i = 0; i < authorities.length; i++)
			Assert.notNull(authorities[i], (new StringBuilder()).append("Granted authority element ").append(i).append(" is null - GrantedAuthority[] cannot contain any null elements").toString());

		setGrantedAuthority(authorities);
		setAccountNonLocked(accountNonLocked);
		setCredentialsNonExpired(credentialsNonExpired);
		setEnabled(enabled);
		setUsername(username);
		setPassword(password);
	}

	public boolean isAccountNonExpired() {
		return isAccountNonExpired;
	}

	public boolean isAccountNonLocked() {
		return isAccountNonLocked;
	}

	public GrantedAuthority[] getAuthorities() {
		return grantedAuthority;
	}

	public boolean isCredentialsNonExpired() {
		return isCredentialsNonExpired;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public GrantedAuthority[] getGrantedAuthority() {
		return grantedAuthority;
	}

	public void setGrantedAuthority(GrantedAuthority grantedAuthority[]) {
		this.grantedAuthority = grantedAuthority;
	}

	public Map getPropiedades() {
		return propiedades;
	}

	public void setPropiedades(Map propiedades) {
		this.propiedades = propiedades;
	}

	public void setAccountNonExpired(boolean isAccountNonExpired) {
		this.isAccountNonExpired = isAccountNonExpired;
	}

	public void setAccountNonLocked(boolean isAccountNonLocked) {
		this.isAccountNonLocked = isAccountNonLocked;
	}

	public void setCredentialsNonExpired(boolean isCredentialsNonExpired) {
		this.isCredentialsNonExpired = isCredentialsNonExpired;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append((new StringBuilder()).append(super.toString()).append(": ").toString());
		sb.append((new StringBuilder()).append("Classname: ").append(getClass().getName()).toString());
		sb.append((new StringBuilder()).append("Username: ").append(username).append("; ").toString());
		sb.append("Password: [PROTECTED]; ");
		sb.append((new StringBuilder()).append("Enabled: ").append(isEnabled).append("; ").toString());
		sb.append((new StringBuilder()).append("AccountNonExpired: ").append(isAccountNonExpired).append("; ").toString());
		sb.append((new StringBuilder()).append("credentialsNonExpired: ").append(isCredentialsNonExpired).append("; ").toString());
		sb.append((new StringBuilder()).append("AccountNonLocked: ").append(isAccountNonLocked).append("; ").toString());
		if (getAuthorities() != null) {
			sb.append("Granted Authorities: ");
			for (int i = 0; i < getAuthorities().length; i++) {
				if (i > 0)
					sb.append(", ");
				sb.append(getAuthorities()[i].toString());
			}

		} else {
			sb.append("Not granted any authorities");
		}
		return sb.toString();
	}
}
